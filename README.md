# SmartHome Simulation

## Úvod

Tento projekt představuje SmartHome simulaci, kde jsou využívány různé design patterny pro usnadnění správy a interakce mezi objekty. Cílem simulace je modelovat chování osob, zařízení a událostí v chytrém domě.


## Použité design patterny:

**Builder:** 
- HouseBuilder, FloorBuilder, RoomBuilder
- vytváření objektů daného typu (House, Floor, Builder)

**Factory:**
- DeviceFactory, StateCotnextFactory
- vytváření objektů Device a StateContext

**State Machine:**
- package -> devices/state
- handling spotřeby daných Device objektů

**Facade:**
- DeviceAPI
- ovládání Device objektů

**Visitor:**
- SimulationVisitorImpl
- navštěvování daných objektů (Event/EntityEvent, LivingEntity, Device, DeviceAPI) pro zapsání záznamu do reportů

**Singleton:**
- DeviceHandler
- má v sobe referenci na Device objekty a jejich vytvořená API pro jednodušší přístup a práce s nimi
- postupem času přidána implementace na správu daných Task objektů

**Observer:**
- ConsumptionObserver/Observable, Observer/Observable
- komunikace mezi objekty implementujícími EventGenerator (Sensor,LivingEntity) a Event objekty pro přehlednější správu a zachytávání eventů



## Funkční požadavky:

- **F1: entity | Částečně splněno** - Lyže a kolo jsou implementovány, jejich využití jako takové však ne, proto osoby využívají hlavně spotřebiče.

- **F2: API na ovládání | Splněno** - DeviceAPI

- **F3: spotřeby ve stavech | Splněno** - State Machine

- **F4: API na sběr dat | Splněno** - DeviceAPI

- **F5: provádění aktivit | Splněno** - Aktivity mají efekt na zařízení v podobě změny spotřeby podle toho, zda je Device objekt používán, stejně tak jako krmení dětí nebo zvířat.

- **F6: nacházení se v místnostech a generování událostí | Splněno** - Entity se pohybují mezi místnostmi a generují události (provádějí aktivity) nebo splňují úkoly, pokud jsou jim přiřazeny.

- **F7: přebírání událostí | Splněno** - Třída EventHandler

- **F8: generování událostí | Částečně splněno** - Generují se všechny události až na HouseConfigurationReport.

- **F9: rozbití zařízení a dokumentace k němu | Splněno** - Pro úkoly vyžadující opravu zařízení se najde osoba s potřebnou inteligencí; není-li žádná nalezena, "zavolá se technik" a zařízení je automaticky opraveno.

- **F10: trávení volného času | Částečně (?) splněno** - (viz F1), osoby využívají zařízení; není-li žádné přístupné, tráví čas volnou aktivitou náhodně vygenerovanou. Zvířata jsou v daný moment buď hladová nebo také generují náhodnou aktivitu.

## Spouštění

Pokoušel jsem se spouštění simulace zprovoznit přímo z příkazové řádky, stejně jako většinu projektů. To se mi však kvůli nesmyslným hláškám nepovedlo, a tak (přestože jsem se problémy snažil vyřešit) nezbývá nic jiného, než projekt spustit přímo z IDE.

Po spuštění je uživateli poskytnut výběr ze 2 konfigurací domu a následně počet požadovaných iterací. Načítání konfigurací domu je spuštěno v třídě HouseConfig z jednoho ze dříve zvolených JSON souborů. V konzoli se následně vypisují důležité hlášky o průběhu simulace, vygenerované události, informace o spotřebě a použití spotřebičů. Po dokončení simulace je vypsáno do tří reportů -> eventReport.txt, consumptionReport.txt, actionReport.txt.

## Obecný popis

Hlavní třída je SmartHome, ve které se spouští simulace jako takové. V ní v každé iteraci probíhá plnění úkolů a generování událostí. Pro zpracování těchto událostí je použita třída EventHandler. O správu komunikace mezi zařízeními a jejich API, stejně jako o přebírání úkolů s nimi spojenými, se stará DeviceHandler. Události mohou generovat objekty rozšiřující třídu LivingEntity nebo Sensor.
