package cz.cvut.fel.omo.sw.devices;

import cz.cvut.fel.omo.sw.exceptions.UnknownDeviceException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DeviceFactoryImplTest {

    @Test
    public void creatingUnknownDeviceShowsError(){
        DeviceFactoryImpl factory = new DeviceFactoryImpl();

        assertThrows(UnknownDeviceException.class, () -> {
            factory.createDevice("Not implemented",1);
        });
    }

    @Test
    public void creatingKnownDevicesReturnsCorrectDevice(){
        DeviceFactoryImpl factory = new DeviceFactoryImpl();

        List<String> devNames = new ArrayList<>();

        String stove = "Stove";
        String fridge = "Fridge";

        devNames.add(stove);
        devNames.add(fridge);

        List<Device> devices = new ArrayList<>();

        for (String device: devNames){
            Device created = factory.createDevice(device,1);

            devices.add(created);
        }

        assertFalse(devices.isEmpty());
        assertEquals(2, devices.size());

    }
}
