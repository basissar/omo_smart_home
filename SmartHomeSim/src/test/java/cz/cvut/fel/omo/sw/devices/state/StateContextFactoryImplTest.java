package cz.cvut.fel.omo.sw.devices.state;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class StateContextFactoryImplTest {

    StateContextFactoryImpl factory = new StateContextFactoryImpl();

//    @Test
//    public void testCreateContextFromCSV_CoffeeMachine() {
//        DeviceStateContext context = factory.createContextFromCSV("Coffee Machine");
//
//        assertNotNull(context);
//        assertEquals(800, context.getActivePowerConsumption());
//        assertEquals(200, context.getActiveWaterConsumption());
//        assertEquals(5, context.getIdlePowerConsumption());
//        assertEquals(0, context.getIdleWaterConsumption());
//        assertEquals(1, context.getOffPowerConsumption());
//        assertEquals(0, context.getOffWaterConsumption());
//        assertEquals(1000, context.getDurability());
//        assertEquals(2.0, context.getDeterioration(), 0.001);
//    }
//
//    @Test
//    public void testCreateContextFromCSV_Computer() {
//        DeviceStateContext context = factory.createContextFromCSV("Computer");
//
//        assertNotNull(context);
//        assertEquals(100, context.getActivePowerConsumption());
//        assertEquals(0, context.getActiveWaterConsumption());
//        assertEquals(10, context.getIdlePowerConsumption());
//        assertEquals(0, context.getIdleWaterConsumption());
//        assertEquals(2, context.getOffPowerConsumption());
//        assertEquals(0, context.getOffWaterConsumption());
//        assertEquals(5000, context.getDurability());
//        assertEquals(1.0, context.getDeterioration(), 0.001);
//    }
//
//    @Test
//    public void testCreateContextFromCSV_Fridge() {
//        DeviceStateContext context = factory.createContextFromCSV("Fridge");
//
//        assertNotNull(context);
//        assertEquals(150, context.getActivePowerConsumption());
//        assertEquals(0, context.getActiveWaterConsumption());
//        assertEquals(20, context.getIdlePowerConsumption());
//        assertEquals(0, context.getIdleWaterConsumption());
//        assertEquals(5, context.getOffPowerConsumption());
//        assertEquals(0, context.getOffWaterConsumption());
//        assertEquals(5000, context.getDurability());
//        assertEquals(1.5, context.getDeterioration(), 0.001);
//    }
//
//    @Test
//    public void testCreateContextFromCSV_Laptop() {
//        DeviceStateContext context = factory.createContextFromCSV("Laptop");
//
//        assertNotNull(context);
//        assertEquals(50, context.getActivePowerConsumption());
//        assertEquals(0, context.getActiveWaterConsumption());
//        assertEquals(5, context.getIdlePowerConsumption());
//        assertEquals(0, context.getIdleWaterConsumption());
//        assertEquals(1, context.getOffPowerConsumption());
//        assertEquals(0, context.getOffWaterConsumption());
//        assertEquals(3000, context.getDurability());
//        assertEquals(2.0, context.getDeterioration(), 0.001);
//    }
//
//    @Test
//    public void testCreateContextFromCSV_Radio() {
//        DeviceStateContext context = factory.createContextFromCSV("Radio");
//
//        assertNotNull(context);
//        assertEquals(20, context.getActivePowerConsumption());
//        assertEquals(0, context.getActiveWaterConsumption());
//        assertEquals(2, context.getIdlePowerConsumption());
//        assertEquals(0, context.getIdleWaterConsumption());
//        assertEquals(1, context.getOffPowerConsumption());
//        assertEquals(0, context.getOffWaterConsumption());
//        assertEquals(4000, context.getDurability());
//        assertEquals(1.2, context.getDeterioration(), 0.001);
//    }
//
//    @Test
//    public void testCreateContextFromCSV_Stove() {
//        DeviceStateContext context = factory.createContextFromCSV("Stove");
//
//        assertNotNull(context);
//        assertEquals(1000, context.getActivePowerConsumption());
//        assertEquals(0, context.getActiveWaterConsumption());
//        assertEquals(10, context.getIdlePowerConsumption());
//        assertEquals(0, context.getIdleWaterConsumption());
//        assertEquals(2, context.getOffPowerConsumption());
//        assertEquals(0, context.getOffWaterConsumption());
//        assertEquals(2000, context.getDurability());
//        assertEquals(3.0, context.getDeterioration(), 0.001);
//    }
//
//    @Test
//    public void testCreateContextFromCSV_TV() {
//        DeviceStateContext context = factory.createContextFromCSV("TV");
//
//        assertNotNull(context);
//        assertEquals(200, context.getActivePowerConsumption());
//        assertEquals(0, context.getActiveWaterConsumption());
//        assertEquals(15, context.getIdlePowerConsumption());
//        assertEquals(0, context.getIdleWaterConsumption());
//        assertEquals(3, context.getOffPowerConsumption());
//        assertEquals(0, context.getOffWaterConsumption());
//        assertEquals(8000, context.getDurability());
//        assertEquals(1.8, context.getDeterioration(), 0.001);
//    }
//
//    @Test
//    public void testCreateContextFromCSV_WashingMachine() {
//        DeviceStateContext context = factory.createContextFromCSV("Washing Machine");
//
//        assertNotNull(context);
//        assertEquals(500, context.getActivePowerConsumption());
//        assertEquals(10, context.getActiveWaterConsumption());
//        assertEquals(30, context.getIdlePowerConsumption());
//        assertEquals(5, context.getIdleWaterConsumption());
//        assertEquals(5, context.getOffPowerConsumption());
//        assertEquals(0, context.getOffWaterConsumption());
//        assertEquals(2000, context.getDurability());
//        assertEquals(2.5, context.getDeterioration(), 0.001);
//    }
//
//    @Test
//    public void testCreateContextFromCSV_Window() {
//        DeviceStateContext context = factory.createContextFromCSV("Window");
//
//        assertNotNull(context);
//        assertEquals(0, context.getActivePowerConsumption());
//        assertEquals(0, context.getActiveWaterConsumption());
//        assertEquals(0, context.getIdlePowerConsumption());
//        assertEquals(0, context.getIdleWaterConsumption());
//        assertEquals(0, context.getOffPowerConsumption());
//        assertEquals(0, context.getOffWaterConsumption());
//        assertEquals(10000, context.getDurability());
//        assertEquals(0.5, context.getDeterioration(), 0.001);
//    }
//
//    @Test
//    public void testCreateContextFromCSV_Blinds() {
//        DeviceStateContext context = factory.createContextFromCSV("Blinds");
//
//        assertNotNull(context);
//        assertEquals(0, context.getActivePowerConsumption());
//        assertEquals(0, context.getActiveWaterConsumption());
//        assertEquals(2, context.getIdlePowerConsumption());
//        assertEquals(0, context.getIdleWaterConsumption());
//        assertEquals(0, context.getOffPowerConsumption());
//        assertEquals(0, context.getOffWaterConsumption());
//        assertEquals(8000, context.getDurability());
//        assertEquals(0.8, context.getDeterioration(), 0.001);
//    }
//
//    @Test
//    public void testCreateContextFromCSV_Light() {
//        DeviceStateContext context = factory.createContextFromCSV("Light");
//
//        assertNotNull(context);
//        assertEquals(15, context.getActivePowerConsumption());
//        assertEquals(0, context.getActiveWaterConsumption());
//        assertEquals(2, context.getIdlePowerConsumption());
//        assertEquals(0, context.getIdleWaterConsumption());
//        assertEquals(1, context.getOffPowerConsumption());
//        assertEquals(0, context.getOffWaterConsumption());
//        assertEquals(5000, context.getDurability());
//        assertEquals(1.5, context.getDeterioration(), 0.001);
//    }
//
//    @Test
//    public void testCreateContextFromCSV_Bike() {
//        DeviceStateContext context = factory.createContextFromCSV("Bike");
//
//        assertNotNull(context);
//        assertEquals(0, context.getActivePowerConsumption());
//        assertEquals(0, context.getActiveWaterConsumption());
//        assertEquals(0, context.getIdlePowerConsumption());
//        assertEquals(0, context.getIdleWaterConsumption());
//        assertEquals(0, context.getOffPowerConsumption());
//        assertEquals(0, context.getOffWaterConsumption());
//        assertEquals(1000, context.getDurability());
//        assertEquals(5.0, context.getDeterioration(), 0.001);
//    }
//
//    @Test
//    public void testCreateContextFromCSV_Ski() {
//        DeviceStateContext context = factory.createContextFromCSV("Ski");
//
//        assertNotNull(context);
//        assertEquals(0, context.getActivePowerConsumption());
//        assertEquals(0, context.getActiveWaterConsumption());
//        assertEquals(0, context.getIdlePowerConsumption());
//        assertEquals(0, context.getIdleWaterConsumption());
//        assertEquals(0, context.getOffPowerConsumption());
//        assertEquals(0, context.getOffWaterConsumption());
//        assertEquals(50, context.getDurability());
//        assertEquals(10.0, context.getDeterioration(), 0.001);
//    }
}
