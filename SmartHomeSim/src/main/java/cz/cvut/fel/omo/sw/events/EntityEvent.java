package cz.cvut.fel.omo.sw.events;

import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.entities.Activity;
import cz.cvut.fel.omo.sw.entities.LivingEntity;
import cz.cvut.fel.omo.sw.entities.Task;
import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;
import cz.cvut.fel.omo.sw.reports.VisitorElement;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public abstract class EntityEvent implements VisitorElement {

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private LivingEntity entity;

    @Getter
    @Setter
    private Activity activity;

    private Task task;

    @Getter
    @Setter
    private List<Device> devices = new ArrayList<>();

    @Getter
    @Setter
    private EventType type;

    public EntityEvent(LivingEntity entity, String name) {
        this.entity = entity;
        this.name = name;
    }

    public void addDevice(Device device){
        this.devices.add(device);
    }

    @Override
    public void accept(SimulationVisitor visitor) {
    }

    @Override
    public void accept(SimulationVisitor visitor, int iteration) {

    }
}
