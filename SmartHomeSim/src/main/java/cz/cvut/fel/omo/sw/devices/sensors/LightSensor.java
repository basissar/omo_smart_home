package cz.cvut.fel.omo.sw.devices.sensors;


import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.devices.DeviceAPI;
import cz.cvut.fel.omo.sw.devices.observer.Observer;
import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.events.EventType;
import cz.cvut.fel.omo.sw.events.sensorEvents.LightEvent;
import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;

import java.util.Random;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:45 PM
 */
public class LightSensor extends Sensor {

	private final Random random = new Random();

	public LightSensor(Room room, int id) {
		super(room, id);
	}

	public LightSensor(int id) {
		super(id);
	}

	/**
	 * Generates event of type {@link EventType#LIGHT_EVENT} and class {@link LightEvent}
	 *
	 * @param visitor {@link cz.cvut.fel.omo.sw.reports.SimulationVisitorImpl} passed to write an entry into the eventReport.txt file
	 * @param iteration int passed for the entry into the eventReport.txt file
	 * @return created {@link LightEvent}
	 */
	@Override
	public Event generateEvent(SimulationVisitor visitor, int iteration) {
		boolean dark = random.nextBoolean();

		LightEvent event = new LightEvent(dark, this.getRoom());
		event.setType(EventType.LIGHT_EVENT);

		System.out.println("Light Sensor with id: " + this.getId() +
				" in room: " + this.getRoom().toStringForSensor()+
				" caught an event " + event.toString());

		notifyObservers(event);

		event.accept(visitor, iteration);
		return event;
	}


	/**
	 * Attaches Observer of class {@link DeviceAPI} but only if the device is light or blinds.
	 * @param observer
	 */
	@Override
	public void attachObserver(Observer observer) {
		Device device = ((DeviceAPI) observer).device;

		if (device.isLight() || device.isBlinds()){
			super.attachObserver(observer);
		}
	}

	@Override
	public void notifyObserver(Observer observer) {
		super.notifyObserver(observer);
	}

}