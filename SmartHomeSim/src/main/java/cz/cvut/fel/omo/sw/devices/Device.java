package cz.cvut.fel.omo.sw.devices;


import cz.cvut.fel.omo.sw.devices.observer.Observer;
import cz.cvut.fel.omo.sw.devices.state.DeviceStateContext;
import cz.cvut.fel.omo.sw.entities.LivingEntity;
import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;
import cz.cvut.fel.omo.sw.reports.VisitorElement;
import cz.cvut.fel.omo.sw.entities.Human;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:40 PM
 */
@Getter
public class Device implements Observer, VisitorElement {

	private static int instanceCount = 0;

	private static final Logger LOG = LoggerFactory.getLogger(Device.class.getSimpleName());

	private final String deviceName;
	private final int id;
	private int powerConsumed;
	private int timesUsed;

	@Getter
	@Setter
	private LivingEntity usedByThisIteration;

	@Setter
	@Getter
	private boolean usedThisIteration;
	private int waterConsumed;

	private DeviceType type;

//	private DeviceAPI deviceAPI;

	private DeviceStateContext stateContext;

	private Manual manual;

	public Device(String deviceName, DeviceType type, DeviceStateContext stateContext) {
		this.manual = new Manual();

        this.id = ++instanceCount;

		this.deviceName = deviceName;
		this.type = type;
		this.stateContext = stateContext;

		DeviceAPI api = new DeviceAPI(this, stateContext);
		DeviceHandler.getInstance().addToMap(api);
	}

	public Device(String name, int id, DeviceStateContext context, DeviceType type){
		this.manual = new Manual();

		this.deviceName = name;
		this.id  = id;
		stateContext = context;
		this.type = type;

		DeviceAPI api = new DeviceAPI(this, stateContext);
		DeviceHandler.getInstance().addToMap(api);
	}
	@Override
	public void accept(SimulationVisitor visitor){
		visitor.visitDevice(this);

		this.usedThisIteration = false;
		this.usedByThisIteration = null;
	}

	@Override
	public void accept(SimulationVisitor visitor, int iteration) {

	}

	/**
	 * Updates the device based on the received event.
	 *
	 * @param event The event triggering the update.
	 */
	@Override
	public void update(Event event) {
		switch (event.getType()) {
			case POWER_EVENT:
				System.out.println("DEVICE UPDATED BASED ON POWER EVENT");
				break;
			case TEMPERATURE_EVENT:
				System.out.println("DEVICE UPDATED BASED ON TEMPERATURE EVENT");
				break;

			default:
				System.out.println("Unknown event type received.");
				break;
		}
	}

	public void addPower(int powerAdded){
		this.powerConsumed += powerAdded;
	}

	public void addWater(int waterAdded){
		this.waterConsumed += waterAdded;
	}

	public boolean isLight(){
		return type == DeviceType.LIGHT;
	}

	public boolean isBlinds(){
		return type == DeviceType.BLINDS;
	}

	public boolean isWindow(){
		return type == DeviceType.WINDOW;
	}

	public boolean isElectronic(){
		return type == DeviceType.ELECTRONIC;
	}

	public boolean isEquipment(){
		return type == DeviceType.EQUIPMENT;
	}

	public void turnedOn(){
		switch (type){
			case BLINDS:
				System.out.println(getDeviceName() + " HAVE BEEN PULLED UP");
				break;
			case WINDOW:
				System.out.println(getDeviceName() + " HAS BEEN OPENED");
				break;
			case ELECTRONIC, LIGHT:
				System.out.println(getDeviceName() + " TURNED ON");
				break;
			case EQUIPMENT:
				System.out.println(getDeviceName() + " HAS BEEN PUT TO USE");
				break;
			default:
				System.out.println("Unknown device type");
				break;
		}
	}

	public void turnedOff(){
		switch (type) {
			case BLINDS:
				System.out.println(getDeviceName() + " HAVE BEEN PULLED DOWN");
				break;
			case WINDOW:
				System.out.println(getDeviceName() + " HAS BEEN CLOSED");
				break;
			case ELECTRONIC, LIGHT:
				System.out.println(getDeviceName() + " TURNED OFF");
				break;
			case EQUIPMENT:
				System.out.println(getDeviceName() + " HAS BEEN PUT AWAY");
				break;
			default:
				System.out.println("Unknown device type");
				break;
		}
	}

	public void alreadyOn(){
		switch (type) {
			case BLINDS:
				System.out.println(getDeviceName() + " IS ALREADY PULLED UP");
				break;
			case WINDOW:
				System.out.println(getDeviceName() + " IS ALREADY OPENED");
				break;
			case ELECTRONIC, LIGHT:
				System.out.println(getDeviceName() + " IS ALREADY TURNED ON");
				break;
            case EQUIPMENT:
				System.out.println(getDeviceName() + " IS ALREADY IN USE");
				break;
			default:
				System.out.println("Unknown device type");
				break;
		}
	}

	public void alreadyOff(){
		switch (type) {
			case BLINDS:
				System.out.println(getDeviceName() + " IS ALREADY PULLED DOWN");
				break;
			case WINDOW:
				System.out.println(getDeviceName() + " IS ALREADY CLOSED");
				break;
			case ELECTRONIC, LIGHT:
				System.out.println(getDeviceName() + " IS ALREADY TURNED OFF");
				break;
            case EQUIPMENT:
				System.out.println(getDeviceName() + " IS ALREADY PUT AWAY");
				break;
			default:
				System.out.println("Unknown device type");
				break;
		}
	}

	@Override
	public String toString() {
		return "Device{" +
				"deviceName='" + deviceName + '\'' +
				", id=" + id +
				", powerConsumed=" + powerConsumed +
				", timesUsed=" + timesUsed +
				", waterConsumed=" + waterConsumed +
				'}';
	}

	public String toStringForState(){
		return "deviceName='" + deviceName + '\'' +
				", id=" + id;
	}
}