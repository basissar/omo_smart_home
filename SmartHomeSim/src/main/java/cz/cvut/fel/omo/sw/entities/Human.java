package cz.cvut.fel.omo.sw.entities;


import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.devices.DeviceAPI;
import cz.cvut.fel.omo.sw.devices.DeviceHandler;
import cz.cvut.fel.omo.sw.environment.Constrains;
import cz.cvut.fel.omo.sw.events.*;
import cz.cvut.fel.omo.sw.events.entityEvents.FreeTimeEvent;
import cz.cvut.fel.omo.sw.events.entityEvents.HungerEvent;
import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;
import cz.cvut.fel.omo.sw.reports.VisitorElement;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static cz.cvut.fel.omo.sw.environment.Constrains.*;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:44 PM
 */
public class Human extends LivingEntity implements VisitorElement {

	@Getter
	private int intelligence;

	@Getter
	private boolean isChild;

	Random random = new Random();

	public static final List<String> ACTIVITIES = Arrays.asList(
			"Read a book",
			"Have a board game night",
			"Listen to music or podcasts",
			"Work on a creative project",
			"Practice a musical instrument",
			"Do indoor exercises or yoga",
			"Solve puzzles or crosswords",
			"Do home workouts",
			"Organize and declutter",
			"Do arts and crafts",
			"Play a musical instrument",
			"Experiment with home science projects",
			"Sleep",
			"Paint",
			"Crochet"
	);

	public Human(int intelligence, String name, int age,  boolean isChild) {
		super(age, name);
		this.intelligence = intelligence;
		this.isChild = isChild;
	}

	public void useDevice(Device device){
		DeviceHandler.useDevice(device, this);
	}

	/**
	 *
	 * @param task
	 */
	public void doTask(Task task){
		Device toFix = task.getDeviceToFix();
		LivingEntity toFeed = task.getEntityToFeed();

		System.out.println(ANSI_PURPLE + "INSIDE DO TASK"+ ANSI_RESET);

		if (toFix != null){
			DeviceHandler.fixDevice(toFix);
		} else if (toFeed != null){
			toFeed.getFed(this);
//			task.setFinished(true);
			DeviceHandler.getInstance().finishTask(toFeed);
		}
	}


	@Override
	public void accept(SimulationVisitor visitor, int iteration) {
		visitor.visitLivingEntity(this, iteration);
	}

	@Override
	public void doActivity(Activity activity){
		if (activity.getDevice() != null){
			useDevice(activity.getDevice());
		}
		setLastAction(activity);
	}

	private boolean inKitchen(){
		return Objects.equals(getCurrentRoom().getName(), "Kitchen");
	}

	@Override
	public void finishActivity() {
		if (getLastAction() instanceof Activity act){
			if (act.getDevice() != null){
				DeviceHandler.turnOffDevice(act.getDevice());
			}
		}
	}

	@Override
	public void moveToRoom(Room room) {
		setCurrentRoom(room);
		System.out.printf("%s Human %s moved to room %s %s \n", Constrains.ANSI_GREEN, getName(), room.getName(), ANSI_RESET);
	}

	/**
	 * Generates and returns an {@link EntityEvent}.
	 *
	 * The generated event depends on whether the entity is in the kitchen or not. If in the kitchen,
	 * a {@link HungerEvent} is generated. Otherwise, the entity interacts with a random device or engages
	 * in a random activity, creating a {@link FreeTimeEvent}. The generated event is then accepted by the
	 * provided {@link SimulationVisitor}, and the event is handled by the {@link EventHandler}.
	 *
	 * @param visitor The simulation visitor to accept the generated event.
	 * @param iteration The current iteration number.
	 * @return The generated {@link EntityEvent} for the entity.
	 */
	@Override
	public EntityEvent generateEntityEvent(SimulationVisitor visitor, int iteration) {
		EntityEvent event = null;
		if (inKitchen()){
			event = new HungerEvent(this);
			event.setType(EventType.HUNGER_EVENT);
		} else {
			Device device = getCurrentRoom().getRandomDevice();

			Activity act = new Activity(getCurrentRoom().getRandomDevice());

			if (device == null){

				String randomActivity = ACTIVITIES.get(random.nextInt(ACTIVITIES.size()));

				act.setName(randomActivity);
			} else {

				act.setName("used device: " + act.getDevice().getDeviceName());
			}

			event = new FreeTimeEvent(this, act.getName());
			event.setActivity(act);
			event.setEntity(this);
			event.setType(EventType.FREE_EVENT);
		}

		EventHandler.handleEntityEvent(event);
		event.accept(visitor, iteration);
		return event;
	}

	@Override
	public String toString() {
		return "Human{" +
				" intelligence= " + intelligence +
				", isChild= " + isChild +
				", name= " + getName() +
				", age= " + getAge() +
				'}';
	}
}