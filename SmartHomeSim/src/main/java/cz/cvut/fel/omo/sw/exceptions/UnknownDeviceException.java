package cz.cvut.fel.omo.sw.exceptions;

public class UnknownDeviceException extends SimulationException{
    public UnknownDeviceException() {
        super();
    }

    public UnknownDeviceException(String message) {
        super(message);
    }

    public UnknownDeviceException(String message, Throwable cause) {
        super(message, cause);
    }

    public static UnknownDeviceException create(String deviceName){
        return new UnknownDeviceException("There is no known create method for device: " +
                deviceName + ". Please check the configuration file." );
    }
}
