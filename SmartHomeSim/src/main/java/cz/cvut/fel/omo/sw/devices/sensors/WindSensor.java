package cz.cvut.fel.omo.sw.devices.sensors;


import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.devices.DeviceAPI;
import cz.cvut.fel.omo.sw.devices.observer.Observer;
import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.events.EventType;
import cz.cvut.fel.omo.sw.events.sensorEvents.TemperatureEvent;
import cz.cvut.fel.omo.sw.events.sensorEvents.WindEvent;
import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:46 PM
 */
public class WindSensor extends Sensor {

	private final Random random = new Random();

	private static final Logger LOG = LoggerFactory.getLogger(WindSensor.class.getSimpleName());


	public WindSensor(Room room, int id) {
		super(room, id);
	}

	public WindSensor(int id) {
		super(id);
	}

	/**
	 * Generates event of type {@link EventType#WIND_EVENT} and class {@link WindEvent}
	 *
	 * @param visitor SimulationVisitorImplementation passed to write an entry into the eventReport.txt file
	 * @param iteration int passed for the entry into the eventReport.txt file
	 * @return created {@link WindEvent}
	 */
	@Override
	public Event generateEvent(SimulationVisitor visitor, int iteration) {
		boolean strongWind = random.nextBoolean();

		WindEvent event = new WindEvent(strongWind, this.getRoom());
		event.setType(EventType.WIND_EVENT);

		System.out.println("Wind Sensor with id: " + this.getId() +
				" in room: " + this.getRoom().toStringForSensor() +
				" caught an event " + event.toString());

		notifyObservers(event);

		event.accept(visitor, iteration);
		return event;
	}

	/**
	 * Attaches Observer of class {@link DeviceAPI} but only if the device is light or electronics.
	 * @param observer
	 */
	@Override
	public void attachObserver(Observer observer) {
		Device device = ((DeviceAPI) observer).device;

		if (device.isBlinds() || device.isWindow()){
			super.attachObserver(observer);
		}
	}

	@Override
	public void notifyAllObservers() {
		super.notifyAllObservers();
	}

	@Override
	public void notifyObserver(Observer observer) {
		super.notifyObserver(observer);
	}
}