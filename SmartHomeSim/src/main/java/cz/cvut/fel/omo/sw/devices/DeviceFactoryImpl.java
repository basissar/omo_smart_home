package cz.cvut.fel.omo.sw.devices;


import cz.cvut.fel.omo.sw.devices.state.DeviceStateContext;
import cz.cvut.fel.omo.sw.devices.state.StateContextFactoryImpl;
import cz.cvut.fel.omo.sw.exceptions.UnknownDeviceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:44 PM
 */
public class DeviceFactoryImpl implements DeviceFactory {


	@Override
	public Device createDevice(String name, int id) {
        return switch (name) {
            case "Coffee Machine" -> createCoffeeMachine(name, id);
            case "Computer" -> createComputer(name, id);
            case "Fridge" -> createFridge(name, id);
            case "Laptop" -> createLaptop(name, id);
            case "Radio" -> createRadio(name, id);
            case "Stove" -> createStove(name, id);
            case "TV" -> createTv(name, id);
            case "Washing Machine" -> createWashingMachine(name, id);
            case "Window" -> createWindow(name, id);
            case "Blinds" -> createBlinds(name, id);
            case "Light" -> createLight(name, id);
            case "Bike" -> createBike(name, id);
            case "Ski" -> createSki(name, id);
			case "Boiler" -> createBoiler(name, id);
			case "Fuses" -> createFuses(name, id);
            default -> {
				throw UnknownDeviceException.create(name);
            }
        };
	}

	private final StateContextFactoryImpl stateFactory = new StateContextFactoryImpl();

	@Override
	public Device createCoffeeMachine(String name, int id) {
		DeviceStateContext context = stateFactory.coffeeMachineContext();
		return new Device(name, id, context,DeviceType.ELECTRONIC);
	}

	@Override
	public Device createComputer(String name, int id) {
		DeviceStateContext context = stateFactory.computerContext();
		return new Device(name, id, context,DeviceType.ELECTRONIC);
	}

	@Override
	public Device createFridge(String name, int id) {
		DeviceStateContext context = stateFactory.fridgeContext();
		return new Device(name, id, context, DeviceType.ELECTRONIC);
	}

	@Override
	public Device createLaptop(String name, int id) {
		DeviceStateContext context = stateFactory.laptopContext();
		return new Device(name, id,  context, DeviceType.ELECTRONIC);
	}

	@Override
	public Device createRadio(String name, int id) {
		DeviceStateContext context = stateFactory.radioContext();
		return new Device(name, id,  context, DeviceType.ELECTRONIC);
	}

	@Override
	public Device createStove(String name, int id) {
		DeviceStateContext context = stateFactory.stoveContext();
		return new Device(name, id, context, DeviceType.ELECTRONIC);
	}

	@Override
	public Device createTv(String name, int id) {
		DeviceStateContext context = stateFactory.tvContext();
		return new Device(name, id, context, DeviceType.ELECTRONIC);
	}

	@Override
	public Device createWashingMachine(String name, int id) {
		DeviceStateContext context = stateFactory.washingMachineContext();
		return new Device(name, id, context, DeviceType.ELECTRONIC);
	}

	@Override
	public Device createWindow(String name, int id) {
		DeviceStateContext context = stateFactory.windowContext();
		return new Device(name, id, context, DeviceType.WINDOW);
	}

	@Override
	public Device createBlinds(String name, int id) {
		DeviceStateContext context = stateFactory.blindsContext();
		return new Device(name, id, context, DeviceType.BLINDS);
	}

	@Override
	public Device createLight(String name, int id) {
		DeviceStateContext context = stateFactory.lightContext();
		return new Device(name, id, context, DeviceType.LIGHT);
	}

	@Override
	public Device createBike(String name, int id) {
		DeviceStateContext context = stateFactory.bikeContext();
		return new Device(name, id, context, DeviceType.EQUIPMENT);
	}

	@Override
	public Device createSki(String name, int id) {
		DeviceStateContext context = stateFactory.skiContext();
		return new Device(name, id, context, DeviceType.EQUIPMENT);
	}

	@Override
	public Device createBoiler(String name, int id) {
		DeviceStateContext context = stateFactory.boilerContext();
		return new Device(name, id, context, DeviceType.ELECTRONIC);
	}

	@Override
	public Device createFuses(String name, int id) {
		DeviceStateContext context = stateFactory.fusesContext();
		return new Device(name, id, context, DeviceType.ELECTRONIC);
	}
}