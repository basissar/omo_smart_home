package cz.cvut.fel.omo.sw.devices.state;


/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:44 PM
 */
public abstract class DeviceState {

	public DeviceStateContext context;

	DeviceState(DeviceStateContext context) {
		this.context = context;
	}

	/**
	 * Puts {@link cz.cvut.fel.omo.sw.devices.Device} into {@link BrokenState}
	 * @param context passed {@link DeviceStateContext} with Device information
	 */
	public abstract void breakDevice(DeviceStateContext context);

	/**
	 * Puts {@link cz.cvut.fel.omo.sw.devices.Device} into {@link IdleState}
	 * @param context passed {@link DeviceStateContext} with Device information
	 */
	public abstract void restMode(DeviceStateContext context);

	/**
	 * Puts {@link cz.cvut.fel.omo.sw.devices.Device} into {@link OffState}
	 * @param context passed {@link DeviceStateContext} with Device information
	 */
	public abstract void turnOff(DeviceStateContext context);

	/**
	 * Puts {@link cz.cvut.fel.omo.sw.devices.Device} into {@link ActiveState}
	 * @param context passed {@link DeviceStateContext} with Device information
	 */
	public abstract void turnOn(DeviceStateContext context);

	public abstract void updateDeviceConsumptions();

}