package cz.cvut.fel.omo.sw.devices.state;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:44 PM
 */
public class IdleState extends DeviceState {

	private static final Logger LOG = LoggerFactory.getLogger(IdleState.class.getSimpleName());

	public IdleState(DeviceStateContext context) {
		super(context);
	}

	@Override
	public void breakDevice(DeviceStateContext context) {
		this.context.setState(new BrokenState(this.context));
		System.out.println("DEVICE BROKEN");
	}

	@Override
	public void restMode(DeviceStateContext context) {
		// already in rest mode
	}

	@Override
	public void turnOff(DeviceStateContext context) {
		this.context.setCurrentPowerConsumption(context.getOffPowerConsumption());
		this.context.setCurrentWaterConsumption(context.getOffWaterConsumption());
		this.context.setState(new OffState(this.context));
		System.out.println("DEVICE TURNED OFF");
	}

	@Override
	public void turnOn(DeviceStateContext context) {
		this.context.setCurrentPowerConsumption(context.getActivePowerConsumption());
		this.context.setCurrentWaterConsumption(context.getActiveWaterConsumption());
		this.context.setState(new ActiveState(this.context));
		System.out.println("DEVICE TURNED ON");
	}

	@Override
	public void updateDeviceConsumptions() {
		// No specific action for updating consumptions in this state
	}

}