package cz.cvut.fel.omo.sw;

import java.io.IOException;
import java.util.Scanner;

public class SmartHome {

    private static String FIRST_HOUSE = "src/main/resources/house.json";

    private static String SECOND_HOUSE = "src/main/resources/house2.json";

    /**
     * The main method representing the entry point of the SmartHome program.
     * It initializes a Simulation object, prompts the user for house configuration and simulation iterations,
     * loads the chosen configuration, and runs the simulation.
     *
     * @param args The command-line arguments (not used in this program).
     * @throws IOException If an IO error occurs while loading the house configuration.
     * @throws InterruptedException If the thread is interrupted during the simulation.
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        Simulation sim = new Simulation();

        System.out.println("Choose a house configuration:");
        System.out.println("1. House 1");
        System.out.println("2. House 2");
        Scanner configScanner = new Scanner(System.in);
        int configChoice = configScanner.nextInt();
        String selectedHouseConfig = (configChoice == 1) ? FIRST_HOUSE : SECOND_HOUSE;

        int iterations = getNumberOfIterations();

        sim.loadConfiguration(selectedHouseConfig);
        sim.run(iterations);
    }

    /**
     * Helper method to get the number of simulation iterations from the user.
     * It prompts the user to enter the number of iterations and ensures the input is a valid integer
     * between 3 and 50 (inclusive).
     *
     * @return The number of simulation iterations specified by the user.
     */
    private static int getNumberOfIterations() {
        Scanner in = new Scanner(System.in);

        int iterations;
        do {
            System.out.print("Enter the number of simulation iterations (3 to 50): ");
            while (!in.hasNextInt()) {
                System.out.print("Invalid input. Please enter a number between 3 and 50: ");
                in.next();
            }
            iterations = in.nextInt();
        } while (iterations < 3 || iterations > 50);

        return iterations;
    }
}
