package cz.cvut.fel.omo.sw.events;

import cz.cvut.fel.omo.sw.reports.SimulationVisitor;

public interface EventGenerator {

    /**
     * Generates an {@link Event} if implemented by {@link cz.cvut.fel.omo.sw.devices.sensors.Sensor}
     * @param visitor passed for Event record into eventReport.txt
     * @param iteration passsed for Event reord in eventReport.txt
     * @return generated Event
     */
    public Event generateEvent(SimulationVisitor visitor, int iteration);

    /**
     * Generates an {@link EntityEvent} if implemented by {@link cz.cvut.fel.omo.sw.entities.LivingEntity}
     * @param visitor
     * @param iteration
     * @return
     */
    public EntityEvent generateEntityEvent(SimulationVisitor visitor, int iteration);
}
