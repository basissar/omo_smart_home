package cz.cvut.fel.omo.sw.devices.observer;

public interface ConsumptionObserver {

    public void update();

}
