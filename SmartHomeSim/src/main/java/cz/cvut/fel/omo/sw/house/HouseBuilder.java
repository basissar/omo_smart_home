package cz.cvut.fel.omo.sw.house;


import cz.cvut.fel.omo.sw.entities.LivingEntity;

import java.util.List;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:44 PM
 */
public class HouseBuilder {

	private House house = new House();

	/**
	 * 
	 * @param floor
	 */
	public HouseBuilder addFloor(Floor floor){
		this.house.addFloor(floor);
		return this;
	}

	public HouseBuilder addFloors(List<Floor> floors){
		this.house.addFloors(floors);
		return this;
	}

	public HouseBuilder setName(String name){
		this.house.setName(name);
		return this;
	}

	public HouseBuilder setEntities(List<LivingEntity> entities){
		this.house.setEntities(entities);
		return this;
	}

	public House getHouse(){
		return house;
	}

}