package cz.cvut.fel.omo.sw.entities;


import cz.cvut.fel.omo.sw.devices.Device;
import lombok.Getter;
import lombok.Setter;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:46 PM
 */
public class Task extends Action {

	@Getter
	private Device deviceToFix;

	@Getter
	private LivingEntity entityToFeed;

	@Getter
	private LivingEntity asignedTo;

	@Getter
	@Setter
	private boolean finished;

	public Task(Device deviceToFix){
		this.deviceToFix = deviceToFix;
	}

	public Task(LivingEntity entityToFeed){
		this.entityToFeed = entityToFeed;
	}

	public void assignEntity(LivingEntity entity){
		asignedTo = entity;
	}

	public int getMinIntelligence(){
		return deviceToFix.getManual().getMinIntelligence();
	}


}