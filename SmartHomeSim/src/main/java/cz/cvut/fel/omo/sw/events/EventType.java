package cz.cvut.fel.omo.sw.events;

public enum EventType {
    POWER_EVENT,
    LIGHT_EVENT,
    TEMPERATURE_EVENT,
    WIND_EVENT,
    ENTITY_EVENT,
    DEVICE_EVENT,

    HUNGER_EVENT,

    FREE_EVENT,

    ANIMAL_EVENT
}
