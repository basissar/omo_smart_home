package cz.cvut.fel.omo.sw.events.entityEvents;

import cz.cvut.fel.omo.sw.entities.LivingEntity;
import cz.cvut.fel.omo.sw.events.EntityEvent;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;

public class FreeTimeEvent extends EntityEvent {
    public FreeTimeEvent(LivingEntity entity, String name) {
        super(entity, name);
    }

    @Override
    public String toString() {
        return ("FREE TIME EVENT ( " + this.getName() + " )");
    }

    @Override
    public void accept(SimulationVisitor visitor, int iteration) {
        visitor.visitEntityEvent(this, iteration);
    }
}
