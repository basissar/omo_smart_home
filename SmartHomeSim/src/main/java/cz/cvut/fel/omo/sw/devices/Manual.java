package cz.cvut.fel.omo.sw.devices;


import lombok.Getter;

import java.util.Random;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:45 PM
 */
@Getter
public class Manual {

	private int fixTime;
	private int minIntelligence;

	private Random random = new Random();

	public Manual(){
		this.fixTime = random.nextInt(1,5);
		minIntelligence = random.nextInt(1,10);
	}



}