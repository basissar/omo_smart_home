package cz.cvut.fel.omo.sw.devices.observer;

import java.util.ArrayList;
import java.util.List;

public abstract class ConsumptionObservable {

    private List<ConsumptionObserver> observers = new ArrayList<>();

    public void attachObserver(ConsumptionObserver observer){
        observers.add(observer);
    }

    public void notifyObservers(){
        for (ConsumptionObserver observer: observers){
            observer.update();
        }
    }

}
