package cz.cvut.fel.omo.sw.house;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.devices.DeviceFactoryImpl;
import cz.cvut.fel.omo.sw.devices.sensors.*;
import cz.cvut.fel.omo.sw.entities.Animal;
import cz.cvut.fel.omo.sw.entities.Human;
import cz.cvut.fel.omo.sw.entities.LivingEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:44 PM
 */
public class HouseConfig {

	private static final String TEMPERATURE_SENSOR_TYPE = "TemperatureSensor";
	private static final String POWER_SENSOR_TYPE = "PowerSensor";
	private static final String WIND_SENSOR_TYPE = "WindSensor";
	private static final String LIGHT_SENSOR_TYPE = "LightSensor";

	public HouseConfig(String filePath) throws IOException {
		this.filePath = filePath;
		loadJson();
	}

	private final String filePath;

	private DeviceFactoryImpl deviceFactory = new DeviceFactoryImpl();

	private List<Device> devices = new ArrayList<>();

	private List<Sensor> sensors = new ArrayList<>();

	public JsonNode rootNode;

	/**
	 * Loads the JSON configuration from the specified file path.
	 *
	 * @throws IOException If an I/O error occurs during JSON file loading.
	 */
	public void loadJson() throws IOException {
		File jsonFile = new File(filePath);

		ObjectMapper mapper = new ObjectMapper();

		rootNode = mapper.readTree(jsonFile);

//		LOG.debug(String.valueOf(rootNode));
	}

	/**
	 * Creates and returns a House object based on the loaded JSON configuration.
	 *
	 * @return The constructed House object.
	 */
	public House createHouse(){
		System.out.println("---- House build started ----");
		HouseBuilder houseBuilder = new HouseBuilder();

		String houseName = rootNode.get("name").asText();

		JsonNode floorsNode = rootNode.get("floors");

		List<Floor> floors = new ArrayList<>();

		if (floorsNode != null && floorsNode.isArray()){
			floors = loadFloors(floorsNode);
		}

		List<LivingEntity> allEntities = new ArrayList<>();

		for (Floor floor: floors){
			allEntities.addAll(floor.getAllEntities());
		}


		System.out.println("-------- House built --------");


        return houseBuilder
				.setName(houseName)
				.addFloors(floors)
				.setEntities(allEntities)
				.getHouse();
	}

	/**
	 * Loads and returns a list of floors from the given JSON node.
	 *
	 * @param node The JSON node representing floors.
	 * @return The list of loaded Floor objects.
	 */
	public List<Floor> loadFloors(JsonNode node){
		List<Floor> result = new ArrayList<>();

		for (JsonNode floorNode: node){
			FloorBuilder floorBuilder = new FloorBuilder();

			int floorNumber = floorNode.get("number").asInt();
			String floorName = floorNode.get("name").asText();

			JsonNode roomsNode = floorNode.get("rooms");

			List<Room> rooms = new ArrayList<>();

			if (roomsNode != null && roomsNode.isArray()){
				rooms = loadRooms(roomsNode);
			}

			Floor newFloor = floorBuilder.setName(floorName)
					.setFloorNumber(floorNumber)
					.addRooms(rooms)
					.getFloor();

			result.add(newFloor);

			System.out.printf("---- Floor %s loaded ---- \n", floorName);
//			LOG.debug(newFloor.toString());
		}

		return result;
	}

	/**
	 * Loads and returns a list of rooms from the given JSON node.
	 *
	 * @param node The JSON node representing rooms.
	 * @return The list of loaded Room objects.
	 */
	public List<Room> loadRooms(JsonNode node){
		List<Room> result = new ArrayList<>();

		for (JsonNode roomNode: node){
			RoomBuilder roomBuilder = new RoomBuilder();

			String roomName = roomNode.get("name").asText();

			JsonNode devicesNode = roomNode.get("devices");

			List<Device> roomDevices = new ArrayList<>();

			if (devicesNode != null && devicesNode.isArray()){
				roomDevices = loadDevices(devicesNode);
			}

			JsonNode sensorsNode = roomNode.get("sensors");

			List<Sensor> roomSensors = new ArrayList<>();

			if (sensorsNode != null && sensorsNode.isArray()){
				roomSensors = loadSensors(sensorsNode);
			}

			JsonNode entityNode = roomNode.get("entity");

			LivingEntity entity=  null;

			if (entityNode != null){
				entity = loadEntity(entityNode);
			}

			Room newRoom = roomBuilder
					.addDevices(roomDevices)
					.setName(roomName)
					.addSensors(roomSensors)
					.addLivingEntity(entity)
					.getRoom();

			result.add(newRoom);

			System.out.printf("---- Room %s loaded ---- \n", roomName );
//			LOG.debug(newRoom.toString());
		}



		return result;
	}

	/**
	 * Loads and returns a list of devices from the given JSON node.
	 *
	 * @param node The JSON node representing devices.
	 * @return The list of loaded Device objects.
	 */
	public List<Device> loadDevices(JsonNode node){
		List<Device> result = new ArrayList<>();

		for (JsonNode deviceNode: node){
			String deviceName = deviceNode.get("name").asText();

			int n = (int) devices.stream()
					.filter(device -> device.getDeviceName()
							.equals(deviceName))
					.count();

			Device createdDevice = deviceFactory.createDevice(deviceName, n + 1);

			devices.add(createdDevice);

			result.add(createdDevice);
		}

		return result;
	}

	/**
	 * Loads and returns a list of sensors from the given JSON node.
	 *
	 * @param node The JSON node representing sensors.
	 * @return The list of loaded Sensor objects.
	 */
	public List<Sensor> loadSensors(JsonNode node){
		List<Sensor> result = new ArrayList<>();

		for (JsonNode sensorNode: node){
//			int sensorId = sensorNode.get("id").asInt();

			String type = sensorNode.get("type").asText();

			int count = (int) sensors.stream()
					.filter(obj -> obj.getClass().getSimpleName().equals(type))
					.count();

			int id = count + 1;

			switch (type) {
				case TEMPERATURE_SENSOR_TYPE:
					sensors.add(new TemperatureSensor(id));

					result.add(new TemperatureSensor(id));
					break;
				case POWER_SENSOR_TYPE:
					sensors.add(new PowerSensor(id));

					result.add(new PowerSensor(id));
					break;
				case WIND_SENSOR_TYPE:
					sensors.add(new WindSensor(id));

					result.add(new WindSensor(id));
					break;
				case LIGHT_SENSOR_TYPE:
					sensors.add(new LightSensor(id));

					result.add(new LightSensor(id));
					break;
				default:
					throw new IllegalArgumentException("Unknown sensor type: " + type);
			}
		}

		return result;
	}

	/**
	 * Loads and returns a LivingEntity from the given JSON node.
	 *
	 * @param node The JSON node representing a LivingEntity.
	 * @return The loaded LivingEntity object.
	 */
	public LivingEntity loadEntity(JsonNode node){
		String name = node.get("name").asText();
		int age = node.get("age").asInt();

		if (node.has("intelligence")) {
			int intelligence = node.get("intelligence").asInt();
			boolean isChild = node.get("isChild").asBoolean();

			Human humanEntity = new Human(intelligence, name, age, isChild);
			System.out.printf("New Human Entity Created: %s\n", humanEntity);
			return humanEntity;
		} else {
			Animal animalEntity = new Animal(age, name);
			System.out.printf("New Animal Entity Created: %s\n", animalEntity);
			return animalEntity;
		}
	}

}