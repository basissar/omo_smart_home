package cz.cvut.fel.omo.sw.events.sensorEvents;


import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:46 PM
 */
public class WindEvent extends Event {

	private boolean STRONG_WIND;


	public WindEvent(String name, Room room) {
		super(name, room);
	}

	public WindEvent(boolean strongWind, Room room){
		super(strongWind ? "STRONG WIND" : "LIGHT WIND", room);
		this.STRONG_WIND = strongWind;
	}

	public String toString(){
		return ("WIND EVENT ( " + this.getName() + " )");
	}

	public boolean isStrong(){
		return STRONG_WIND;
	}

	@Override
	public void accept(SimulationVisitor visitor, int iteration) {
		visitor.visitEvent(this, iteration);
	}
}