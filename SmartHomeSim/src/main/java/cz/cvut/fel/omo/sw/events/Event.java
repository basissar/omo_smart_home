package cz.cvut.fel.omo.sw.events;


import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;
import cz.cvut.fel.omo.sw.reports.VisitorElement;
import lombok.Getter;
import lombok.Setter;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:44 PM
 */
@Getter
public abstract class Event implements VisitorElement {

	private String name;

	private int id;

	@Getter
	@Setter
	private EventType type;

	private Room room;

	public Event(String name, Room room) {
		this.name = name;
		this.room = room;
	}

	@Override
	public void accept(SimulationVisitor visitor) {

	}

	@Override
	public void accept(SimulationVisitor visitor, int iteration) {

	}
}