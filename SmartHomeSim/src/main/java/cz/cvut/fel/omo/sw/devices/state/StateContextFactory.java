package cz.cvut.fel.omo.sw.devices.state;

public interface StateContextFactory {

    public DeviceStateContext coffeeMachineContext();

    public DeviceStateContext computerContext();

    public DeviceStateContext fridgeContext();

    public DeviceStateContext laptopContext();

    public DeviceStateContext radioContext();

    public DeviceStateContext stoveContext();

    public DeviceStateContext tvContext();

    public DeviceStateContext washingMachineContext();

    public DeviceStateContext windowContext();

    public DeviceStateContext blindsContext();

    public DeviceStateContext lightContext();

    public DeviceStateContext bikeContext();

    public DeviceStateContext skiContext();

    public DeviceStateContext boilerContext();

    public DeviceStateContext fusesContext();
}
