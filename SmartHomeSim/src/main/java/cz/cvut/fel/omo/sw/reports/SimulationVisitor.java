package cz.cvut.fel.omo.sw.reports;


import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.devices.DeviceAPI;
import cz.cvut.fel.omo.sw.entities.LivingEntity;
import cz.cvut.fel.omo.sw.events.EntityEvent;
import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.house.Room;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:46 PM
 */
public interface SimulationVisitor {

	/**
	 * 
	 * @param device
	 */
	public void visitDevice(Device device);

	/**
	 * 
	 * @param deviceAPI
	 */
	public void visitDeviceAPI(DeviceAPI deviceAPI);

	/**
	 * @param entity
	 * @param iteration
	 */
	public void visitLivingEntity(LivingEntity entity, int iteration);

	public void visitEntityEvent(EntityEvent event, int iteration);

	public void visitEvent(Event event, int iteration);


}