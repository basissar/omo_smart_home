package cz.cvut.fel.omo.sw.devices.state;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static cz.cvut.fel.omo.sw.environment.Constrains.ANSI_RED;
import static cz.cvut.fel.omo.sw.environment.Constrains.ANSI_RESET;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:40 PM
 */
public class BrokenState extends DeviceState {

	private static final Logger LOG = LoggerFactory.getLogger(BrokenState.class.getSimpleName());

	public BrokenState(DeviceStateContext context) {
		super(context);
	}

	@Override
	public void breakDevice(DeviceStateContext context) {
		// No specific action for breaking in this state
	}

	@Override
	public void restMode(DeviceStateContext context) {
		System.out.println(ANSI_RED + "BROKEN DEVICE CANNOT BE PUT INTO REST MODE" + ANSI_RESET);
	}

	@Override
	public void turnOff(DeviceStateContext context) {
		// No specific action for turning off in this state
	}

	@Override
	public void turnOn(DeviceStateContext context) {
		System.out.println(ANSI_RED + "BROKEN DEVICE CANNOT BE TURNED ON" + ANSI_RESET);
	}

	@Override
	public void updateDeviceConsumptions() {
		// No specific action for updating consumptions in this state
	}

}