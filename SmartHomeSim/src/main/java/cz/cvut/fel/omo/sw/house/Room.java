package cz.cvut.fel.omo.sw.house;


import cz.cvut.fel.omo.sw.devices.*;
import cz.cvut.fel.omo.sw.devices.sensors.*;
import cz.cvut.fel.omo.sw.entities.LivingEntity;
import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.events.EventType;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;
import cz.cvut.fel.omo.sw.reports.VisitorElement;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static cz.cvut.fel.omo.sw.events.EventType.*;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:45 PM
 */
@Getter
public class Room implements VisitorElement {

	private String name;

	private List<Device> devices = new ArrayList<>();

	//might just be one entity as for  one entity per room
	private List<LivingEntity> entities = new ArrayList<>();

	private LivingEntity entity = null;

	public boolean canEnter = true;

	private List<Sensor> sensors = new ArrayList<>();

	private List<Event> events = new ArrayList<>();

	private final List<EventType> eventTypes = Arrays.asList(POWER_EVENT, EventType.TEMPERATURE_EVENT, EventType.WIND_EVENT, EventType.LIGHT_EVENT);


	public void setName(String name){
		this.name = name;
	}

	public void addDevice(Device device){
		this.devices.add(device);
	}

	public void addDevice(List<Device> devices){
		this.devices.addAll(devices);
	}

	public void addEntity(LivingEntity entity){
		this.entities.add(entity);

		this.canEnter = false;

		this.entity = entity;
	}

	public void addEntity(List<LivingEntity> entities){
		this.entities.addAll(entities);
	}

	public void addSensor(Sensor sensor){
		sensor.setRoom(this);

		for (Device device: devices){
			DeviceAPI api = DeviceHandler.getInstance().getApi(device);
			sensor.attachObserver(api);
		}

		this.sensors.add(sensor);
	}

	public void addSensors(List<Sensor> sensors){
		for (Sensor sensor: sensors){
			addSensor(sensor);
		}
	}

	public void addEvent(Event event){
		this.events.add(event);
	}

	public void addEvent(List<Event> events){
		this.events.addAll(events);
	}

	public boolean isOccupied(){
		return this.entity == null;
	}

	/**
	 * Retrieves a random electronic device (excluding "Fuses") from the list of devices.
	 *
	 * @return A random electronic device, or {@code null} if no eligible device is found.
	 */
	public Device getRandomDevice() {
		List<Device> electronicDevices = devices.stream()
				.filter(device -> device.getType() == DeviceType.ELECTRONIC && !device.getDeviceName().equals("Fuses"))
				.toList();

		return electronicDevices.isEmpty()
				? null
				: electronicDevices.get(new Random().nextInt(electronicDevices.size()));
	}

	/**
	 * Finds and returns a device with the specified name.
	 *
	 * @param deviceName The name of the device to find.
	 * @return The device with the specified name, or {@code null} if not found.
	 */
	public Device findDevice(String deviceName){
		return devices.stream()
				.filter(device -> device.getDeviceName().equals(deviceName))
				.findFirst()
				.orElse(null);
	}

	@Override
	public void accept(SimulationVisitor visitor) {
		//todo if needed
	}

	@Override
	public void accept(SimulationVisitor visitor, int iteration) {
		//todo if needed
	}

	@Override
	public String toString() {
		return "Room{" +
				"name='" + name + '\'' +
				", devices=" + devices +
				", sensors=" + sensors +
				'}';
	}

	public String toStringForSensor(){
		return name;
	}

	public int getRandomNumber(int min, int max) {
		return (int) ((Math.random() * (max - min)) + min);
	}
}