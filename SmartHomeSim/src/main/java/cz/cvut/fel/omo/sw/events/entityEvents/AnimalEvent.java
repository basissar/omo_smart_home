package cz.cvut.fel.omo.sw.events.entityEvents;

import cz.cvut.fel.omo.sw.entities.LivingEntity;
import cz.cvut.fel.omo.sw.events.EntityEvent;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;

public class AnimalEvent extends EntityEvent {
    public AnimalEvent(LivingEntity entity, String name) {
        super(entity, name);
    }

    @Override
    public void accept(SimulationVisitor visitor, int iteration) {
        visitor.visitEntityEvent(this, iteration);
    }

    @Override
    public String toString() {
        return ("ANIMAL EVENT ( " + this.getName() + " )");
    }
}
