package cz.cvut.fel.omo.sw.devices.state;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:45 PM
 */
public class OffState extends DeviceState {

	private static final Logger LOG = LoggerFactory.getLogger(OffState.class.getSimpleName());

	public OffState(DeviceStateContext context) {
		super(context);
	}

	@Override
	public void breakDevice(DeviceStateContext context) {
		this.context.setState(new BrokenState(this.context));
		System.out.println("DEVICE: " + context.deviceAPI.device.toStringForState() + " BROKEN" + "||| off state");
	}

	@Override
	public void restMode(DeviceStateContext context) {
		// System.out.println("DEVICE: " + context.deviceAPI.device.toStringForState() + " IN OFF STATE. CANNOT BE PUT INTO REST MODE");
		// device in rest mode cannot be put into Off Mode
	}

	@Override
	public void turnOff(DeviceStateContext context) {
		context.getDevice().alreadyOff();
	}

	@Override
	public void turnOn(DeviceStateContext context) {
		this.context.setCurrentPowerConsumption(context.getActivePowerConsumption());
		this.context.setCurrentWaterConsumption(context.getActiveWaterConsumption());
		this.context.setState(new ActiveState(this.context));

		context.getDevice().turnedOn();
	}

	@Override
	public void updateDeviceConsumptions() {
	}

}