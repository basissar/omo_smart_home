package cz.cvut.fel.omo.sw.devices.sensors;


import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.devices.DeviceAPI;
import cz.cvut.fel.omo.sw.devices.observer.Observer;
import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.events.EventType;
import cz.cvut.fel.omo.sw.events.sensorEvents.PowerEvent;
import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:45 PM
 */
public class PowerSensor extends Sensor {

	private static final Logger LOG = LoggerFactory.getLogger(PowerSensor.class.getSimpleName());

	public PowerSensor(Room room, int id) {
		super(room, id);
	}

	public PowerSensor(int id) {
		super(id);
	}

	/**
	 * Generates event of type {@link EventType#POWER_EVENT} and class {@link PowerEvent}
	 *
	 * @param visitor SimulationVisitorImplementation passed to write an entry into the eventReport.txt file
	 * @param iteration int passed for the entry into the eventReport.txt file
	 * @return created {@link PowerEvent}
	 */
	@Override
	public Event generateEvent(SimulationVisitor visitor, int iteration) {
		PowerEvent event = new PowerEvent("POWER LOSS", this.getRoom());
		event.setType(EventType.POWER_EVENT);

		System.out.println("Power Sensor with id: " + this.getId() +
				" in room: " + this.getRoom().toStringForSensor() +
				" caught an event " + event.toString());

		notifyObservers(event);

		event.accept(visitor, iteration);
		return event;
	}

	/**
	 * Attaches Observer of class {@link DeviceAPI} but only if the device is light or electronics.
	 * @param observer
	 */
	@Override
	public void attachObserver(Observer observer) {
		Device device = ((DeviceAPI) observer).device;

		if (device.isElectronic() || device.isLight()){
			super.attachObserver(observer);
		}
	}

	@Override
	public void notifyObserver(Observer observer) {
		super.notifyObserver(observer);
	}
}