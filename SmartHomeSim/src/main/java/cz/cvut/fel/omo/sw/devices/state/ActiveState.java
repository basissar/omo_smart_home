package cz.cvut.fel.omo.sw.devices.state;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:40 PM
 */
public class ActiveState extends DeviceState {

	public ActiveState(DeviceStateContext context) {
		super(context);
	}

	@Override
	public void breakDevice(DeviceStateContext context) {
		this.context.setState(new BrokenState(this.context));
		System.out.println("DEVICE " + context.getDevice() + " BROKEN");
	}

	@Override
	public void restMode(DeviceStateContext context) {
		this.context.setCurrentPowerConsumption(context.getIdlePowerConsumption());
		this.context.setCurrentWaterConsumption(context.getIdleWaterConsumption());
		this.context.setState(new IdleState(this.context));
		System.out.println("DEVICE " + context.getDevice() + " PUT INTO REST MODE");
	}

	@Override
	public void turnOff(DeviceStateContext context) {
		this.context.setCurrentPowerConsumption(context.getOffPowerConsumption());
		this.context.setCurrentWaterConsumption(context.getOffWaterConsumption());
		this.context.setState(new OffState(this.context));

		context.getDevice().turnedOff();
	}

	@Override
	public void turnOn(DeviceStateContext context) {
		context.getDevice().alreadyOn();
	}

	@Override
	public void updateDeviceConsumptions() {
		// Implementation for updating device consumptions if needed
	}

}