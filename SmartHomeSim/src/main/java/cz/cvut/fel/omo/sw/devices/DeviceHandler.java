package cz.cvut.fel.omo.sw.devices;

import cz.cvut.fel.omo.sw.entities.LivingEntity;
import cz.cvut.fel.omo.sw.entities.Task;
import cz.cvut.fel.omo.sw.environment.Constrains;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cz.cvut.fel.omo.sw.environment.Constrains.ANSI_RESET;

public class DeviceHandler {

    private static DeviceHandler instance;

    private static Map<Device, DeviceAPI> deviceMap = new HashMap<>();

    @Getter
    private static List<Task> tasks = new ArrayList<>();

    private DeviceHandler() {
        // Private constructor to prevent instantiation outside this class
    }

    public static DeviceHandler getInstance() {
        if (instance == null) {
            instance = new DeviceHandler();
        }
        return instance;
    }

    public DeviceHandler(Map<Device, DeviceAPI> deviceMap) {
        this.deviceMap = deviceMap;
    }

    public void addToMap(DeviceAPI api){
        deviceMap.put(api.device, api);
    }

    public void createTask(Task task){
        tasks.add(task);
    }

    public void removeFromMap(DeviceAPI api){
        deviceMap.remove(api.device);
    }

    public DeviceAPI getApi(Device device){
        return deviceMap.get(device);
    }

    public List<DeviceAPI> getAllApis(){
        return new ArrayList<>(deviceMap.values());
    }

    public List<Device> getAllDevices(){
        return new ArrayList<>(deviceMap.keySet());
    }

    public List<Task> getAllTasks(){
        return tasks;
    }

    public static void useDevice(Device device, LivingEntity entity){
        System.out.println(Constrains.ANSI_RED + " DEVICE " + device + " IN USE BY" + entity.getName() + ANSI_RESET);

        device.setUsedByThisIteration(entity);

        device.setUsedThisIteration(true);

        DeviceAPI api = deviceMap.get(device);

        if (api != null) {
            api.turnOn();
        } else {
            System.out.println(Constrains.ANSI_PURPLE + "Device not found in the map." + ANSI_RESET);
        }
    }

    public static void turnOffDevice(Device device){
        DeviceAPI api = deviceMap.get(device);
        api.turnOff();
    }

    public static void fixDevice(Device device){
        DeviceAPI api = deviceMap.get(device);
        api.fixDevice();
    }

    /**
     * Fixes the device without the need of a {@link cz.cvut.fel.omo.sw.entities.Human} entity
     * @param device
     */
    public static void callTechnician(Device device){
        DeviceAPI api = deviceMap.get(device);
        api.fixDevice();
        System.out.println("Technician was called to fix device: " + device.getDeviceName() +
                " with id: " + device.getId());
    }

    public static void finishTask(Device device){
        for (Task task: tasks){
            if(task.getDeviceToFix() != null && task.getDeviceToFix().equals(device)){
                task.setFinished(true);
                break;
            }
        }
    }

    public static void finishTask(LivingEntity entity){
        for (Task task: tasks){
            if (task.getEntityToFeed() != null && task.getEntityToFeed().equals(entity)){
                task.setFinished(true);
                break;
            }
        }
    }
}
