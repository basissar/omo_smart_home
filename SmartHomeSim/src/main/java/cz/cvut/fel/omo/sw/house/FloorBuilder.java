package cz.cvut.fel.omo.sw.house;


import java.util.List;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:44 PM
 */
public class FloorBuilder {

	private Floor floor = new Floor();

	/**
	 * 
	 * @param room
	 */
	public FloorBuilder addRoom(Room room){
		this.floor.addRoom(room);
		return this;
	}

	public FloorBuilder addRooms(List<Room> rooms){
		this.floor.addRooms(rooms);
		return this;
	}

	/**
	 * 
	 * @param floorNumber
	 */
	public FloorBuilder setFloorNumber(int floorNumber){
		floor.setNumber(floorNumber);
		return this;
	}

	public FloorBuilder setName(String name){
		floor.setName(name);
		return this;
	}

	public Floor getFloor(){
		return floor;
	}

}