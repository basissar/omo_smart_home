package cz.cvut.fel.omo.sw.reports;


import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.devices.DeviceAPI;
import cz.cvut.fel.omo.sw.entities.Human;
import cz.cvut.fel.omo.sw.entities.LivingEntity;
import cz.cvut.fel.omo.sw.events.EntityEvent;
import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.house.Room;
import lombok.Getter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:46 PM
 */
public class SimulationVisitorImpl implements SimulationVisitor {

	private String consumptionReport = "consumptionReport.txt";

	@Getter
	private String actionReport = "actionReport.txt";

	@Getter
	private String entityReport = "entityReport.txt";

	@Getter
	private String eventReport = "eventReport.txt";

	private Map<Device, Map<LivingEntity, Integer>> usageMap = new HashMap<>();

	public SimulationVisitorImpl(){

	}

	public String getConsumptionReport() {
		return consumptionReport;
	}

	/**
	 * Visits a device during the simulation. Records device usage if it was used during the iteration.
	 *
	 * @param device The device to visit.
	 */
	public void visitDevice(Device device){
		if (device.isUsedThisIteration()){
			recordUsage(device, device.getUsedByThisIteration());
		}
	}

	/**
	 * Records the usage of a device by a living entity.
	 *
	 * @param device The device being used.
	 * @param human  The living entity using the device.
	 */
	public void recordUsage(Device device, LivingEntity human){
		usageMap.computeIfAbsent(device, k -> new HashMap<>());

		Map<LivingEntity, Integer> innerMap = usageMap.get(device);

		innerMap.merge(human, 1, Integer::sum);
	}

	/**
	 * Creates the action report file and writes device usage details to it.
	 */
	public void createActionReport() {
		try (PrintWriter writer = new PrintWriter(new FileWriter(actionReport))) {
			for (Map.Entry<Device, Map<LivingEntity, Integer>> entry : usageMap.entrySet()) {
				Device device = entry.getKey();
				Map<LivingEntity, Integer> innerMap = entry.getValue();

				writer.println("Device: " + device);

				for (Map.Entry<LivingEntity, Integer> innerEntry : innerMap.entrySet()) {
					LivingEntity entity = innerEntry.getKey();
					int count = innerEntry.getValue();

					writer.println("  - Entity: " + entity + ", Usage Count: " + count);
				}

				writer.println();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Visits a DeviceAPI during the simulation. Writes power and water consumption details to the consumption report file.
	 *
	 * @param api The DeviceAPI to visit.
	 */
	public void visitDeviceAPI(DeviceAPI api){
		try (PrintWriter writer = new PrintWriter(new FileWriter(consumptionReport, true))) {
			writer.print("Device: " + api.device.getDeviceName() + " ID: " + api.device.getId() +  ", ");
			writer.print("Power Consumed: " + api.getPowerConsumed() + ", ");
			writer.println("Water Consumed: " + api.getWaterConsumed());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Visits a living entity during the simulation. Writes entity actions to the entity report file.
	 *
	 * @param entity    The living entity to visit.
	 * @param iteration The current simulation iteration.
	 */
	public void visitLivingEntity(LivingEntity entity, int iteration){
		try (PrintWriter writer = new PrintWriter(new FileWriter(entityReport, true))) {
			writer.println("Iteration: " + iteration + "Entity: " + entity + ", Action: " +
					entity.getLastAction());
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	/**
	 * Visits an entity event during the simulation. Writes event details to the event report file.
	 *
	 * @param event     The entity event to visit.
	 * @param iteration The current simulation iteration.
	 */
	public void visitEntityEvent(EntityEvent event, int iteration){
		try (PrintWriter writer = new PrintWriter(new FileWriter(eventReport, true))){
			writer.println("Iteration: " + iteration + " | EntityEvent: " + event.toString() +
					" | Entity: " + event.getEntity());
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	/**
	 * Visits a general event during the simulation. Writes event details to the event report file.
	 *
	 * @param event     The event to visit.
	 * @param iteration The current simulation iteration.
	 */
	public void visitEvent(Event event, int iteration){
		try (PrintWriter writer = new PrintWriter(new FileWriter(eventReport, true))){
			writer.println("Iteration: " + iteration + " | SensorEvent: " + event.toString() +
					" | Sensor: " );
		} catch (IOException e){
			e.printStackTrace();
		}
	}

}