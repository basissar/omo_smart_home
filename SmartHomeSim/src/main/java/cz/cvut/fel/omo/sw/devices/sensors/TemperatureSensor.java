package cz.cvut.fel.omo.sw.devices.sensors;


import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.devices.DeviceAPI;
import cz.cvut.fel.omo.sw.devices.observer.Observer;
import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.events.EventType;
import cz.cvut.fel.omo.sw.events.sensorEvents.TemperatureEvent;
import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;

import java.util.Random;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:46 PM
 */
public class TemperatureSensor extends Sensor {

	private final Random random = new Random();

	public TemperatureSensor(int id) {
		super(id);
	}

	public TemperatureSensor(Room room, int id) {
		super(room, id);
	}

	/**
	 * Generates event of type {@link EventType#TEMPERATURE_EVENT} and class {@link TemperatureEvent}
	 *
	 * @param visitor SimulationVisitorImplementation passed to write an entry into the eventReport.txt file
	 * @param iteration int passed for the entry into the eventReport.txt file
	 * @return created {@link TemperatureEvent}
	 */
	@Override
	public Event generateEvent(SimulationVisitor visitor, int iteration) {

		boolean highTemp = random.nextBoolean();

		TemperatureEvent event = new TemperatureEvent(highTemp, this.getRoom());
		event.setType(EventType.TEMPERATURE_EVENT);

		System.out.println("Temperature Sensor with id: " + this.getId() +
				" in room: " + this.getRoom().toStringForSensor() +
				" caught an event " + event.toString());

		notifyObservers(event);

		event.accept(visitor, iteration);
		return event;
	}

	/**
	 * Attaches Observer of class {@link DeviceAPI} but only if the device is light or electronics.
	 * @param observer
	 */
	@Override
	public void attachObserver(Observer observer) {
		Device device = ((DeviceAPI) observer).device;

		if (device.isBlinds() || device.isWindow()){
			super.attachObserver(observer);
		}
	}

	@Override
	public void notifyAllObservers() {
		super.notifyAllObservers();
	}

	@Override
	public void notifyObserver(Observer observer) {
		super.notifyObserver(observer);
	}
}