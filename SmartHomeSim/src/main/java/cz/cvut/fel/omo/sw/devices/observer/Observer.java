package cz.cvut.fel.omo.sw.devices.observer;


import cz.cvut.fel.omo.sw.events.Event;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:45 PM
 */
public interface Observer {

	public void update(Event event);

}