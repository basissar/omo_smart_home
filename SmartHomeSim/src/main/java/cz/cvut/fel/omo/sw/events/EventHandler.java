package cz.cvut.fel.omo.sw.events;

import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.devices.DeviceAPI;
import cz.cvut.fel.omo.sw.devices.DeviceHandler;
import cz.cvut.fel.omo.sw.entities.Activity;
import cz.cvut.fel.omo.sw.entities.Human;
import cz.cvut.fel.omo.sw.entities.LivingEntity;
import cz.cvut.fel.omo.sw.entities.Task;
import cz.cvut.fel.omo.sw.events.entityEvents.AnimalEvent;
import cz.cvut.fel.omo.sw.events.entityEvents.FreeTimeEvent;
import cz.cvut.fel.omo.sw.events.entityEvents.HungerEvent;
import cz.cvut.fel.omo.sw.events.sensorEvents.LightEvent;
import cz.cvut.fel.omo.sw.events.sensorEvents.TemperatureEvent;
import cz.cvut.fel.omo.sw.events.sensorEvents.WindEvent;

import java.util.Random;

public class EventHandler {

    private static Random random = new Random();

    /**
     * Handles API-related events for devices.
     *
     * @param event The API-related event to handle.
     * @param api The DeviceAPI associated with the event.
     */
    public static void handleApiEvent(Event event, DeviceAPI api){
        switch (event.getType()){
            case POWER_EVENT -> handlePowerEvent(api);
            case LIGHT_EVENT -> handleLightEvent((LightEvent) event, api);
            case TEMPERATURE_EVENT -> handleTemperatureEvent((TemperatureEvent) event, api);
            case WIND_EVENT -> handleWindEvent((WindEvent) event, api);
            default -> System.out.println("Unknown event type received");
        }
    }

    /**
     * Handles entity-related events.
     *
     * @param event The entity-related event to handle.
     */
    public static void handleEntityEvent(EntityEvent event){
        switch (event.getType()){
            case HUNGER_EVENT -> handleHungerEvent((HungerEvent) event);
            case ANIMAL_EVENT -> handleAnimalEvent((AnimalEvent) event);
            case FREE_EVENT -> handleFreeEvent((FreeTimeEvent) event);
            default -> System.out.println("Unknown event type received");
        }
    }

    /**
     * Handles {@link cz.cvut.fel.omo.sw.events.sensorEvents.PowerEvent} events for devices.
     *
     * @param api The {@link DeviceAPI} associated with the event.
     */
    private static void handlePowerEvent( DeviceAPI api){
        if (api.device.isLight() || api.device.isElectronic()){
            api.turnOff();
        }
    }

    /**
     * Handles {@link cz.cvut.fel.omo.sw.events.sensorEvents.LightEvent} events for devices.
     *
     * @param api The {@link DeviceAPI} associated with the event.
     */
    private static void handleLightEvent(LightEvent event, DeviceAPI api){
        if (event.isDark()){
            if(api.device.isBlinds()){
                api.turnOff();
            } else if (api.device.isLight()) {
                api.turnOn();
            }
        } else {
            if (api.device.isBlinds()){
                api.turnOn();
            } else if (api.device.isLight()){
                api.turnOff();
            }
        }
    }

    /**
     * Handles {@link cz.cvut.fel.omo.sw.events.sensorEvents.TemperatureEvent} events for devices.
     *
     * @param api The {@link DeviceAPI} associated with the event.
     */
    private static void handleTemperatureEvent(TemperatureEvent event, DeviceAPI api){
        if (event.isHigh()){
            api.turnOn();
        } else {
            api.turnOff();
        }
    }

    /**
     * Handles {@link cz.cvut.fel.omo.sw.events.sensorEvents.WindEvent} events for devices.
     *
     * @param api The {@link DeviceAPI} associated with the event.
     */
    private static void handleWindEvent(WindEvent event, DeviceAPI api){
        if (event.isStrong()){
            if (api.device.isBlinds()){
                api.turnOn();
            } else if (api.device.isWindow()){
                api.turnOff();
            }
        }
    }

    /**
     * Handles {{@link HungerEvent}} events for entities.
     *
     * @param event The {@link HungerEvent} event to handle.
     */
    private static void handleHungerEvent(HungerEvent event){
        if (event.getEntity() instanceof Human human){

            if (!human.isChild()){
                Device fridge = human.getCurrentRoom().findDevice("Fridge");
                Device stove = human.getCurrentRoom().findDevice("Stove");

                int n = random.nextInt(2);

                Activity act;
                if (n == 0){
                    act = new Activity(fridge);
                    act.setName("got food from " + fridge.getDeviceName());
                } else {
                    act = new Activity(stove);
                    act.setName("cooked food on " + stove.getDeviceName());
                }
                human.doActivity(act);
            }
        } else {
            Task newTask = new Task(event.getEntity());
            DeviceHandler.getInstance().createTask(newTask);
        }

    }

    private static void handleAnimalEvent(AnimalEvent event){
        //possible future implementation
    }

    /**
     * Handles {{@link FreeTimeEvent}} events for entities.
     *
     * @param event The {@link FreeTimeEvent} event to handle.
     */
    private static void handleFreeEvent(FreeTimeEvent event){
        Human human = (Human) event.getEntity();
        human.doActivity(event.getActivity());
    }

}
