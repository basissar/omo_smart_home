package cz.cvut.fel.omo.sw.house;


import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.entities.LivingEntity;
import cz.cvut.fel.omo.sw.devices.sensors.Sensor;

import java.util.List;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:45 PM
 */
public class RoomBuilder {

	private Room room = new Room();

	public RoomBuilder setName(String name){
		room.setName(name);
		return this;
	}

	/**
	 * 
	 * @param device
	 */
	public RoomBuilder addDevice(Device device){
		room.addDevice(device);
		return this;
	}

	public RoomBuilder addDevices(List<Device> devices){
		room.addDevice(devices);
		return this;
	}

	/**
	 * 
	 * @param entity
	 */
	public RoomBuilder addLivingEntity(LivingEntity entity){
		room.addEntity(entity);
		return this;
	}

	public RoomBuilder addLivingEntities(List<LivingEntity> entities){
		room.addEntity(entities);
		return this;
	}

	public RoomBuilder addSensors(List<Sensor> sensors){
		room.addSensors(sensors);
		return this;
	}

	public Room getRoom(){
		return room;
	}

}