package cz.cvut.fel.omo.sw.entities;


import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.events.EventGenerator;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;
import cz.cvut.fel.omo.sw.reports.SimulationVisitorImpl;
import cz.cvut.fel.omo.sw.reports.VisitorElement;
import cz.cvut.fel.omo.sw.house.Room;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:45 PM
 */
public abstract class LivingEntity implements VisitorElement, EventGenerator {

	@Getter
	private int age;

	@Getter
	private String name;

	@Setter
	@Getter
	private Room currentRoom;

	@Getter
	@Setter
	private Action lastAction;

	public LivingEntity(int age, String name) {
		this.age = age;
		this.name = name;
	}

	public void getFed(Human human){
	}

	/**
	 *
	 * @param visitor
	 */
	public void accept(SimulationVisitor visitor){

	}

	/**
	 * 
	 * @param activity
	 */
	public void doActivity(Activity activity){
	}

	/**
	 * Moves entity to {@link Room} provided in the method parameter
	 * @param room
	 */
	public void moveToRoom(Room room){

	}

	@Override
	public Event generateEvent(SimulationVisitor visitor, int iteration) {
		return null;
	}

	public void finishActivity() {
	}
}