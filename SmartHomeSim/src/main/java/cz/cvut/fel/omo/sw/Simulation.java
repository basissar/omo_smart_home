package cz.cvut.fel.omo.sw;


import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.devices.DeviceAPI;
import cz.cvut.fel.omo.sw.devices.DeviceHandler;
import cz.cvut.fel.omo.sw.devices.observer.ConsumptionObservable;
import cz.cvut.fel.omo.sw.devices.observer.ConsumptionObserver;
import cz.cvut.fel.omo.sw.devices.sensors.Sensor;
import cz.cvut.fel.omo.sw.entities.Action;
import cz.cvut.fel.omo.sw.entities.Human;
import cz.cvut.fel.omo.sw.entities.LivingEntity;
import cz.cvut.fel.omo.sw.entities.Task;
import cz.cvut.fel.omo.sw.environment.Constrains;
import cz.cvut.fel.omo.sw.events.EntityEvent;
import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.events.EventHandler;
import cz.cvut.fel.omo.sw.house.House;
import cz.cvut.fel.omo.sw.house.HouseConfig;
import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitorImpl;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

import static cz.cvut.fel.omo.sw.environment.Constrains.ANSI_RESET;
import static cz.cvut.fel.omo.sw.environment.Constrains.ANSI_YELLOW;
import static java.lang.Thread.sleep;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:46 PM
 */
@Getter
public class Simulation extends ConsumptionObservable {

	private House house;

	private SimulationVisitorImpl visitor = new SimulationVisitorImpl();

	Random random = new Random();

	public Simulation(){
	}

	/**
	 * Runs the simulation for a specified number of iterations.
	 *
	 * @param iterations The number of iterations to run the simulation.
	 * @throws InterruptedException If the thread is interrupted during sleep.
	 */
	public void run(int iterations) throws InterruptedException {
		System.out.println(Constrains.ANSI_RED + "----- SIMULATION STARTED -----" + ANSI_RESET );
		int iteration = 1;

		while (iteration <= iterations) {
			sleep(500);

			iterate(iteration);

			iteration++;
		}

		System.out.println(Constrains.ANSI_RED + "----- SIMULATION FINISHED -----" + ANSI_RESET );

		generateReports();
	}

	/**
	 * Loads the house configuration from a JSON file.
	 *
	 * @param path The path to the JSON configuration file.
	 * @throws IOException If an IO error occurs while loading the configuration.
	 */
	public void loadConfiguration(String path) throws IOException {
		HouseConfig conf = new HouseConfig(path);
		this.house = conf.createHouse();

		List<DeviceAPI> apis = DeviceHandler.getInstance().getAllApis();

		for (DeviceAPI api: apis){
			this.attachObserver(api);
		}
	}

	//todo remove this later just a test impl
	public Room getRoom(String name){
		List<Room> rooms = house.getAllRooms();

		return rooms.stream()
				.filter(room -> room.getName().equals(name))
				.findFirst().get();
	}

	/**
	 * Performs a single iteration of the simulation, including entity movements,
	 * task execution, event generation, and report recording.
	 *
	 * @param n The current iteration number.
	 * @throws InterruptedException If the thread is interrupted during sleep.
	 */
	public void iterate(int n) throws InterruptedException {
		System.out.println(Constrains.ANSI_RED + "----- ITERATION NUMBER " + n + " -----" + ANSI_RESET);

		moveEntities();

		sleep(500);

		List<Task> tasks = DeviceHandler.getInstance().getAllTasks();

		if (!tasks.isEmpty()){

			for (Task task: tasks){
				if (!task.isFinished()){
					if (task.getDeviceToFix() != null){
						int necessaryIntelligence = task.getMinIntelligence();

						Human human  = house.findAdult(necessaryIntelligence);

						if (human == null){
							DeviceHandler.getInstance().callTechnician(task.getDeviceToFix());
						} else {
							human.doTask(task);
						}
					} else {
						Human human = house.findAnyAdult();

						human.doTask(task);
					}
				}
			}
		}

		generateEntityEvent(n);


		for (int i = 0; i < 5 ; i++) {
			notifyObservers();
		}

		finishEntityActivities();

		recordUsages();

		generateRandomEvents(n);
	}

	/**
	 * Moves entities between rooms randomly, ensuring each room is entered only once.
	 */
	public void moveEntities(){
		List<Room> rooms = house.getAllRooms();

		rooms.forEach(room -> room.canEnter = true);

		Collections.shuffle(rooms);

		for (Room room : rooms) {
			LivingEntity entity = room.getEntity();

			if (entity != null){
				Room roomToEnter = rooms.stream()
						.filter(r -> r.canEnter && !Objects.equals(r, room))
						.findFirst()
						.orElse(room);

				entity.moveToRoom(roomToEnter);
				roomToEnter.canEnter = false;
			}
		}
	}

	/**
	 * Generates a random event in a random room with a random sensor.
	 *
	 * @param iteration The current iteration number.
	 * @throws InterruptedException If the thread is interrupted during sleep.
	 */
	public void generateRandomEvents(int iteration) throws InterruptedException {
		List<Room> rooms = house.getAllRooms();

		Room room = rooms.get(random.nextInt(rooms.size()));

		List<Sensor> sensors = room.getSensors();

		Sensor sensor = sensors.get(random.nextInt(sensors.size()));

		sensor.generateEvent(visitor, iteration);

		sleep(700);
	}

	/**
	 * Generates entity events for all living entities in the house.
	 *
	 * @param iteration The current iteration number.
	 */
	public void generateEntityEvent(int iteration){
		List<Room> rooms = house.getAllRooms();

		for (Room room: rooms){
			LivingEntity entity = room.getEntity();

			if (entity != null){
				entity.generateEntityEvent(visitor, iteration);
			}
		}
	}

	/**
	 * Finishes activities for all living entities in the house.
	 */
	public void finishEntityActivities(){
		List<Room> rooms = house.getAllRooms();
		for (Room room: rooms){
			if (room.getEntity() != null){
				room.getEntity().finishActivity();
			}
		}
	}

	/**
	 * Records the usages of all devices in the house.
	 */
	public void recordUsages(){
		List<Device> devices = DeviceHandler.getInstance().getAllDevices();

		for (Device device: devices){
			device.accept(visitor);
		}
	}

	/**
	 * Generates consumption and action reports after the simulation.
	 */
	public void generateReports(){
		generateConsumptionReport();
		generateActionReport();
	}

	/**
	 * Generates the consumption report by visiting all device APIs.
	 */
	public void generateConsumptionReport(){
		List<DeviceAPI> devices = DeviceHandler.getInstance().getAllApis();

		for (DeviceAPI api: devices){
			api.accept(visitor);
		}

		System.out.println(ANSI_YELLOW + "CONSUMPTION REPORT GENERATED TO FILE: " + visitor.getConsumptionReport() + ANSI_RESET);
	}

	/**
	 * Generates the action report using the visitor's createActionReport method.
	 */
	public void generateActionReport(){
		visitor.createActionReport();

		System.out.println(ANSI_YELLOW + "ACTION REPORT GENERATED TO FILE: " + visitor.getActionReport() + ANSI_RESET);
	}

	@Override
	public void notifyObservers() {
		super.notifyObservers();
	}

	@Override
	public void attachObserver(ConsumptionObserver observer) {
		super.attachObserver(observer);
	}
}