package cz.cvut.fel.omo.sw.devices;


/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:43 PM
 */
public interface DeviceFactory {

	public Device createDevice(String name, int id);

	public Device createCoffeeMachine(String name, int id);

	public Device createComputer(String name, int id);

	public Device createFridge(String name, int id);

	public Device createLaptop(String name, int id);

	public Device createRadio(String name, int id);

	public Device createStove(String name, int id);

	public Device createTv(String name, int id);

	public Device createWashingMachine(String name, int id);

	public Device createWindow(String name, int id);

	public Device createBlinds(String name, int id);

	public Device createLight(String name, int id);

	public Device createBike(String name, int id);

	public Device createSki(String name, int id);

	public Device createBoiler(String name, int id);

	public Device createFuses(String name, int id);

}