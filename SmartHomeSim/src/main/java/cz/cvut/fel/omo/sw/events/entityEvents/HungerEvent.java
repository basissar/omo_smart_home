package cz.cvut.fel.omo.sw.events.entityEvents;

import cz.cvut.fel.omo.sw.entities.Activity;
import cz.cvut.fel.omo.sw.entities.LivingEntity;
import cz.cvut.fel.omo.sw.entities.Task;
import cz.cvut.fel.omo.sw.events.EntityEvent;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;

public class HungerEvent extends EntityEvent {
    public HungerEvent(LivingEntity entity) {
        super(entity, entity.getName() + " is hungry");
        setEntity(entity);
    }

    @Override
    public String toString() {
        return String.format("ENTITY EVENT ( %s )", this.getName());
    }

    @Override
    public void accept(SimulationVisitor visitor, int iteration) {
        visitor.visitEntityEvent(this, iteration);
    }
}
