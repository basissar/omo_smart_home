package cz.cvut.fel.omo.sw.exceptions;

public class SimulationException extends RuntimeException{

    public SimulationException() {
    }

    public SimulationException(String message) {
        super(message);
    }

    public SimulationException(String message, Throwable cause) {
        super(message, cause);
    }

    public SimulationException(Throwable cause) {
        super(cause);
    }
}
