package cz.cvut.fel.omo.sw.house;


import cz.cvut.fel.omo.sw.entities.Human;
import cz.cvut.fel.omo.sw.entities.LivingEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:44 PM
 */
@Getter
@Setter
public class House {

	private String name;

	private List<Floor> floors = new ArrayList<>();

	private List<LivingEntity> entities = new ArrayList<>();

	/**
	 * Finds and returns any {@link Human} with the attribute {@code isChild} set to {@code false}.
	 *
	 * @return A {@link Human} object that is not a child, or {@code null} if none is found.
	 */
	public Human findAnyAdult(){
		Human foundHuman = null;

		for (LivingEntity entity: entities){
			if (entity instanceof Human){
				Human check = (Human) entity;

				if (!check.isChild()){
					foundHuman = check;
					break;
				}
			}
		}

		return foundHuman;
	}

	/**
	 * Finds and returns a {@link Human} with intelligence equal to or greater than the specified value,
	 * who is not a child.
	 *
	 * @param intelligence The minimum intelligence level to search for.
	 * @return A {@link Human} object meeting the specified criteria, or {@code null} if none is found.
	 */
	public Human findAdult(int intelligence){
		Human foundHuman = null;

		for (LivingEntity entity: entities){
			if (entity instanceof Human){
				Human check = (Human) entity;

				if (!check.isChild() && (check.getIntelligence() >= intelligence)){
					foundHuman = check;
					break;
				}
			}
		}

		return foundHuman;
	}

	public void addFloor(Floor floor){
		this.floors.add(floor);
	}

	public void addFloors(List<Floor> floors){
		this.floors.addAll(floors);
	}

	public List<Room> getAllRooms(){
		List<Room> allRooms = new ArrayList<>();

		for (Floor floor: floors) {
			allRooms.addAll(floor.getRooms());
		}

		return allRooms;
	}

}