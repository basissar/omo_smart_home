package cz.cvut.fel.omo.sw.events.sensorEvents;


import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:45 PM
 */
public class LightEvent extends Event {

	private boolean DARK;

	public LightEvent(String name, Room room) {
		super(name, room);
	}

	public LightEvent(boolean darkness, Room room){
		super(darkness ? "IT'S DARK" : "LET THERE BE LIGHT", room);
		this.DARK = darkness;
	}

	public String toString(){
		return ("LIGHT EVENT ( " + this.getName() + " )");
	}

	public boolean isDark() {
		return DARK;
	}

	@Override
	public void accept(SimulationVisitor visitor, int iteration) {
		visitor.visitEvent(this, iteration);
	}
}