package cz.cvut.fel.omo.sw.exceptions;

public class TypeMismatchException extends SimulationException {
    public TypeMismatchException(String message) {
        super(message);
    }

    public static TypeMismatchException create(String eventType, String eventClass){
        return new TypeMismatchException("Event type and class instance do not match. | EventType: " +
                eventType + " | EventClass: " + eventClass);
    }
}
