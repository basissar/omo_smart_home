package cz.cvut.fel.omo.sw.devices.observer;


import cz.cvut.fel.omo.sw.events.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:45 PM
 */
public abstract class Observable {

	private List<Observer> observers = new ArrayList<>();

	/**
	 * @param observer
	 */
	public void attachObserver(Observer observer) {
		observers.add(observer);
	}

	public void notifyAllObservers() {

	}

	public void notifyObservers(Event event) {
		for (Observer observer: observers) {
			observer.update(event);
		}
	}

	/**
	 * @param observer
	 */
	public void notifyObserver(Observer observer) {

	}

}