package cz.cvut.fel.omo.sw.exceptions;

public class ConfigNotFoundException extends SimulationException{

    public ConfigNotFoundException(String message) {
        super(message);
    }

    public ConfigNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public static ConfigNotFoundException create(String resourceName, Object identifier) {
        return new ConfigNotFoundException("Configuration with name " + resourceName
                + "of class " + identifier + " not found.");
    }
}
