package cz.cvut.fel.omo.sw.events.sensorEvents;


import cz.cvut.fel.omo.sw.environment.Generator;
import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:46 PM
 */
public class TemperatureEvent extends Event {

	private boolean HIGH_TEMPERATURE;

	private double temp;

	public boolean isHigh(){
		return HIGH_TEMPERATURE;
	}

	public TemperatureEvent(boolean highTemperature, Room room) {
		super(highTemperature ? "HIGH TEMPERATURE" : "LOW TEMPERATURE", room);
		this.setTemperature(highTemperature);
	}


	public void setTemperature(boolean HIGH_TEMPERATURE) {
		this.HIGH_TEMPERATURE = HIGH_TEMPERATURE;

		if (HIGH_TEMPERATURE){
			this.temp = Generator.randomDouble(27.0, 40);
		} else {
			this.temp = Generator.randomDouble(18, 22);
		}
	}

	public String toString() {
		return String.format("TEMPERATURE EVENT (%s %.2f°)", this.getName(), this.temp);
	}

	@Override
	public void accept(SimulationVisitor visitor, int iteration) {
		visitor.visitEvent(this, iteration);
	}
}