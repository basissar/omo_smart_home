package cz.cvut.fel.omo.sw.house;


import cz.cvut.fel.omo.sw.entities.LivingEntity;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:44 PM
 */
@Getter
public class Floor {

	private int number;

	private String name;

	private List<Room> rooms = new ArrayList<>();

	public void addRoom(Room room){
		rooms.add(room);
	}

	public void addRooms(List<Room> rooms){
		this.rooms.addAll(rooms);
	}

	public void setNumber(int n){
		this.number = n;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<LivingEntity> getAllEntities(){
		List<LivingEntity> result = new ArrayList<>();

		for (Room room: rooms){
			result.addAll(room.getEntities());
		}

		return result;
	}
}