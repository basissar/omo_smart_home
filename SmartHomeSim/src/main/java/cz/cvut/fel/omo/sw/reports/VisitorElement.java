package cz.cvut.fel.omo.sw.reports;


/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:46 PM
 */
public interface VisitorElement {

	/**
	 * Accepts a {@link SimulationVisitor}.
	 *
	 * @param visitor The simulation visitor.
	 */
	public void accept(SimulationVisitor visitor);

	/**
	 * Accepts a {@link SimulationVisitor} with a specified iteration.
	 *
	 * @param visitor The simulation visitor.
	 * @param iteration The iteration number.
	 */
	public void accept(SimulationVisitor visitor, int iteration);

}