package cz.cvut.fel.omo.sw.devices;

public enum DeviceType {
    LIGHT,
    BLINDS,
    WINDOW,
    ELECTRONIC,
    EQUIPMENT,

    HEATER
}
