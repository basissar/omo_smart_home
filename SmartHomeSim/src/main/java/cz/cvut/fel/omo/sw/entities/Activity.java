package cz.cvut.fel.omo.sw.entities;


import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.entities.Action;
import lombok.Getter;
import lombok.Setter;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:40 PM
 */
public class Activity extends Action {

	@Getter
	private Device device;

	@Getter
	@Setter
	private String name;

	public Activity(Device device){
		this.device = device;
	}

	@Override
	public String toString() {
		return "Activity{" +
				" name='" + name + '\'' +
				'}';
	}
}