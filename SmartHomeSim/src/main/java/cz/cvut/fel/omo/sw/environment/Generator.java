package cz.cvut.fel.omo.sw.environment;

public class Generator {

    public static double randomDouble(double min, double max){
        return min + (max - min) * Math.random();
    }
}
