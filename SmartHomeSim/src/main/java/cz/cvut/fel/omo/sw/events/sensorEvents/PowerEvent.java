package cz.cvut.fel.omo.sw.events.sensorEvents;


import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;

import java.util.List;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:45 PM
 */
public class PowerEvent extends Event {

	public PowerEvent(String name, Room room) {
		super(name, room);
	}

	public String toString(){
		return ("POWER EVENT ( " + this.getName() + " )");
	}

	@Override
	public void accept(SimulationVisitor visitor, int itereation) {
		visitor.visitEvent(this, itereation);
	}
}