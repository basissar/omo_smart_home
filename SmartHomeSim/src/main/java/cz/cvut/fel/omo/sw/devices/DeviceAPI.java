package cz.cvut.fel.omo.sw.devices;


import ch.qos.logback.core.pattern.color.ANSIConstants;
import cz.cvut.fel.omo.sw.devices.observer.ConsumptionObserver;
import cz.cvut.fel.omo.sw.devices.observer.Observer;
import cz.cvut.fel.omo.sw.devices.state.ActiveState;
import cz.cvut.fel.omo.sw.devices.state.DeviceStateContext;
import cz.cvut.fel.omo.sw.devices.state.IdleState;
import cz.cvut.fel.omo.sw.entities.Task;
import cz.cvut.fel.omo.sw.events.*;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;
import cz.cvut.fel.omo.sw.reports.VisitorElement;


/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:40 PM
 */
public class DeviceAPI implements Observer, ConsumptionObserver, VisitorElement {

	public Device device;

	public DeviceStateContext context;

	public DeviceAPI(Device device, DeviceStateContext context) {
		this.device = device;
		context.setDeviceAPI(this);
		this.context = context;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public void setContext(DeviceStateContext context) {
		this.context = context;
	}

	public void getDurability(){
	}

	public int getPowerConsumed(){
		return this.device.getPowerConsumed();
	}

	public int getWaterConsumed(){
		return this.device.getWaterConsumed();
	}

	/**
	 * Breaks device
	 * Sets the State to {@link cz.cvut.fel.omo.sw.devices.state.BrokenState}
	 */
	public void breakDevice(){
		this.context.getState().breakDevice(context);

		Task newTask = new Task(device);

		DeviceHandler.getInstance().createTask(newTask);
	}

	/**
	 * Fixes devices
	 * Sets the state to {@link cz.cvut.fel.omo.sw.devices.state.OffState}
	 */
	public void fixDevice(){
		this.context.getState().turnOff(context);
		this.context.setCurrentDurability(context.getDurability());

		DeviceHandler.getInstance().finishTask(device);
//		DeviceHandler.getInstance().removeTask(device);
	}

	/**
	 * Sets the state to {@link IdleState}
	 */
	public void restMode(){
		this.context.getState().restMode(context);
	}

	/**
	 * Sets the state to {@link cz.cvut.fel.omo.sw.devices.state.OffState}
	 */
	public void turnOff(){
		this.context.getState().turnOff(context);
	}

	/**
	 * Sets the state to {@link ActiveState}
	 */
	public void turnOn(){
		this.context.getState().turnOn(context);
	}

	public void updatePowerConsumption(){
		int consumption = context.getCurrentPowerConsumption();

		device.addPower(consumption);
	}

	public void updateWaterConsumption(){
		int consumption = context.getCurrentWaterConsumption();

		device.addWater(consumption);
	}

	public void updateDurability(){
		if (context.isActive()){
			context.setCurrentDurability((context.getCurrentDurability() - context.getDeterioration()));
		} else if (context.isIdle()){
			context.setCurrentDurability(context.getCurrentDurability() - (context.getDeterioration()/2));
		}

		if (context.getCurrentDurability() <= 10){
			breakDevice();
		}
	}

	public void updateDeviceConsumptions(){
		updatePowerConsumption();
		updateWaterConsumption();
	}

	@Override
	public void update() {
		updateDeviceConsumptions();
		updateDurability();
	}

	@Override
	public void accept(SimulationVisitor visitor) {
		visitor.visitDeviceAPI(this);
	}

	@Override
	public void accept(SimulationVisitor visitor, int iteration) {

	}

	@Override
	public void update(Event event) {
		EventHandler.handleApiEvent(event, this);
	}
}