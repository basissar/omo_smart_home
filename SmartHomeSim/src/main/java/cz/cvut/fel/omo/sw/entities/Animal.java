package cz.cvut.fel.omo.sw.entities;


import cz.cvut.fel.omo.sw.entities.LivingEntity;
import cz.cvut.fel.omo.sw.events.EntityEvent;
import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.events.EventHandler;
import cz.cvut.fel.omo.sw.events.EventType;
import cz.cvut.fel.omo.sw.events.entityEvents.AnimalEvent;
import cz.cvut.fel.omo.sw.events.entityEvents.FreeTimeEvent;
import cz.cvut.fel.omo.sw.events.entityEvents.HungerEvent;
import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static cz.cvut.fel.omo.sw.environment.Constrains.*;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:40 PM
 */
public class Animal extends LivingEntity {

	public static final List<String> ACTIVITIES = Arrays.asList(
			"Run around",
			"Play with toys",
			"Explore the surroundings",
			"Rest in the shade",
			"Groom",
			"Chase tail",
			"Take a nap",
			"Sunbathe",
			"Huddle for warmth",
			"Jump and hop",
			"Pounce on toys",
			"Sniff and explore"
	);

	private Random random = new Random();
	private boolean isHungry;

	public Animal(int age, String name) {
		super(age, name);
		this.isHungry = false;
	}

	public boolean isHungry() {
		return isHungry;
	}

	public void setHungry(boolean hungry) {
		isHungry = hungry;
	}

	@Override
	public void getFed(Human human){
		setHungry(false);
		System.out.println(ANSI_PURPLE + getName() + " got fed by: " + human.getName()+ ANSI_RESET);
	}

	@Override
	public void finishActivity() {
		super.finishActivity();
	}

	@Override
	public void accept(SimulationVisitor visitor, int iteration) {

	}

	/**
	 * Generates and returns an {@link EntityEvent} for the animal during simulation.
	 *
	 * The generated event is based on a random number, determining whether the animal is hungry or
	 * engaged in a random activity. The generated event is then accepted by the provided
	 * {@link SimulationVisitor}, and the event is handled by the {@link EventHandler}.
	 *
	 * @param visitor The simulation visitor to accept the generated event.
	 * @param iteration The current iteration number.
	 * @return The generated {@link EntityEvent} for the animal.
	 */
	@Override
	public EntityEvent generateEntityEvent(SimulationVisitor visitor, int iteration) {

		int n = random.nextInt(100);

		if (n < 30){
			this.isHungry = true;

			HungerEvent event = new HungerEvent(this);
			event.setType(EventType.HUNGER_EVENT);

			System.out.println("Animal: " + this.getName() + " generated an event: " + event);

			event.accept(visitor, iteration);
			EventHandler.handleEntityEvent(event);
			return event;
		} else {
			int ranInd = random.nextInt(ACTIVITIES.size());
			String activity = ACTIVITIES.get(ranInd);
			AnimalEvent event = new AnimalEvent(this, activity);

			event.setType(EventType.ANIMAL_EVENT);

			event.accept(visitor, iteration);
			EventHandler.handleEntityEvent(event);
			return event;
		}
	}

	@Override
	public void moveToRoom(Room room) {
		setCurrentRoom(room);
		System.out.printf("%s Animal %s moved to room %s %s \n", ANSI_GREEN, getName(), room.getName(), ANSI_RESET);
	}

	@Override
	public String toString() {
		return "Animal{" +
				" name= " + getName() +
				" age= " + getAge() +
				" isHungry= " + isHungry +
				'}';
	}
}