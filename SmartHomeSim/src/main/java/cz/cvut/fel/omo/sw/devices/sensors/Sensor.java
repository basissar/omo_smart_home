package cz.cvut.fel.omo.sw.devices.sensors;


import cz.cvut.fel.omo.sw.devices.observer.Observable;
import cz.cvut.fel.omo.sw.devices.observer.Observer;
import cz.cvut.fel.omo.sw.events.EntityEvent;
import cz.cvut.fel.omo.sw.events.Event;
import cz.cvut.fel.omo.sw.events.EventGenerator;
import cz.cvut.fel.omo.sw.house.Room;
import cz.cvut.fel.omo.sw.reports.SimulationVisitor;
import cz.cvut.fel.omo.sw.reports.SimulationVisitorImpl;
import lombok.Getter;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:46 PM
 */
@Getter
public abstract class Sensor extends Observable implements EventGenerator {

	private int id;
	private Room room;

	Sensor(Room room, int id) {
		this.id = id;
		this.room = room;
	}

	Sensor(int id){
		this.id = id;
	}

	public void setRoom(Room room) {
		this.room = room;
	}


	@Override
	public Event generateEvent(SimulationVisitor visitor, int iteration) {
		return null;
	}


	@Override
	public void notifyObserver(Observer observer) {

	}

	@Override
	public EntityEvent generateEntityEvent(SimulationVisitor visitor, int iteration) {
		return null;
	}
}