package cz.cvut.fel.omo.sw.devices.state;


import cz.cvut.fel.omo.sw.devices.Device;
import cz.cvut.fel.omo.sw.devices.DeviceAPI;
import lombok.Getter;
import lombok.Setter;

/**
 * @author sbasi
 * @version 1.0
 * @created 15-Nov-2023 2:32:44 PM
 */

@Getter
@Setter
public class DeviceStateContext {

	private int activePowerConsumption;
	private int activeWaterConsumption;
	private double deterioration;
	private double durability;
	private double currentDurability;
	private int idlePowerConsumption;
	private int idleWaterConsumption;
	private int offPowerConsumption;
	private int offWaterConsumption;

	public DeviceState deviceState;

	private int currentWaterConsumption;
	private int currentPowerConsumption;

	public DeviceAPI deviceAPI;

	public DeviceStateContext(){
		deviceState = new OffState(this);
	}

	public void setDeviceAPI(DeviceAPI deviceAPI) {
		this.deviceAPI = deviceAPI;
	}

	public String getDeviceName(){
		return this.deviceAPI.device.getDeviceName();
	}

	public Device getDevice(){
		return this.deviceAPI.device;
	}

	public boolean deviceIsBlinds(){
		return this.deviceAPI.device.isBlinds();
	}

	public boolean deviceIsLight(){
		return this.deviceAPI.device.isLight();
	}

	public boolean deviceIsWindow(){
		return this.deviceAPI.device.isWindow();
	}

	public boolean deviceIsElectronic(){ return this.deviceAPI.device.isElectronic(); }

	public boolean deviceIsEquipment(){
		return this.deviceAPI.device.isEquipment();
	}

	public void setState(DeviceState state){
		deviceState = state;
	}

	public DeviceState getState(){
		return deviceState;
	}

	public boolean isActive(){
		return deviceState instanceof ActiveState;
	}

	public boolean isIdle(){
		return deviceState instanceof IdleState;
	}

	@Override
	public String toString() {
		return "DeviceStateContext{" +
				"activePowerConsumption=" + activePowerConsumption +
				", activeWaterConsumption=" + activeWaterConsumption +
				", deterioration=" + deterioration +
				", durability=" + durability +
				", idlePowerConsumption=" + idlePowerConsumption +
				", idleWaterConsumption=" + idleWaterConsumption +
				", offPowerConsumption=" + offPowerConsumption +
				", offWaterConsumption=" + offWaterConsumption +
				'}';
	}
}