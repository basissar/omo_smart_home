package cz.cvut.fel.omo.sw.devices.state;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;

public class StateContextFactoryImpl implements StateContextFactory{

    private static final String CSV_FILE_PATH = "stateContext.csv";

    @Override
    public DeviceStateContext coffeeMachineContext() {
        return createContextFromCSV("Coffee Machine");
    }

    @Override
    public DeviceStateContext computerContext() {
        return createContextFromCSV("Computer");
    }

    @Override
    public DeviceStateContext fridgeContext() {
        return createContextFromCSV("Fridge");
    }

    @Override
    public DeviceStateContext laptopContext() {
        return createContextFromCSV("Laptop");
    }

    @Override
    public DeviceStateContext radioContext() {
        return createContextFromCSV("Radio");
    }

    @Override
    public DeviceStateContext stoveContext() {
        return createContextFromCSV("Stove");
    }

    @Override
    public DeviceStateContext tvContext() {
        return createContextFromCSV("TV");
    }

    @Override
    public DeviceStateContext washingMachineContext() {
        return createContextFromCSV("Washing Machine");
    }

    @Override
    public DeviceStateContext windowContext() {
        return createContextFromCSV("Window");
    }

    @Override
    public DeviceStateContext blindsContext() {
        return createContextFromCSV("Blinds");
    }

    @Override
    public DeviceStateContext lightContext() {
        return createContextFromCSV("Light");
    }

    @Override
    public DeviceStateContext bikeContext() {
        return createContextFromCSV("Bike");
    }

    @Override
    public DeviceStateContext skiContext() {
        return createContextFromCSV("Ski");
    }

    @Override
    public DeviceStateContext boilerContext() {
        return createContextFromCSV("Boiler");
    }

    @Override
    public DeviceStateContext fusesContext() {
        return createContextFromCSV("Fuses");
    }

    /**
     * Creates a {@link DeviceStateContext} from a CSV file for the specified device name.
     *
     * @param deviceName The name of the device.
     * @return The context for the device, or null if not found or on error.
     */
    public DeviceStateContext createContextFromCSV(String deviceName) {
        try (FileReader fileReader = new FileReader(CSV_FILE_PATH);
             CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT.withHeader())) {

            for (CSVRecord rec : csvParser) {
                if (rec.get("deviceName").trim().equalsIgnoreCase(deviceName.trim())) {
                    return createStateContext(rec);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Creates a {@link DeviceStateContext} from a {@link CSVRecord}.
     *
     * Constructs a {@link DeviceStateContext} using the information provided in the given CSV record.
     * The initial current durability is set equal to the overall durability.
     *
     * @param rec The CSV record for context creation.
     * @return A {@link DeviceStateContext} with state information from the CSV record.
     */
    public DeviceStateContext createStateContext(CSVRecord rec) {
        DeviceStateContext context = new DeviceStateContext();
        context.setActivePowerConsumption(Integer.parseInt(rec.get("APC").trim()));
        context.setActiveWaterConsumption(Integer.parseInt(rec.get("AWC").trim()));
        context.setIdlePowerConsumption(Integer.parseInt(rec.get("IPC").trim()));
        context.setIdleWaterConsumption(Integer.parseInt(rec.get("IWC").trim()));
        context.setOffPowerConsumption(Integer.parseInt(rec.get("OPC").trim()));
        context.setOffWaterConsumption(Integer.parseInt(rec.get("OWC").trim()));
        context.setDurability(Integer.parseInt(rec.get("durability").trim()));
        context.setDeterioration(Double.parseDouble(rec.get("deterioration").trim()));

        context.setCurrentDurability(context.getDurability());
        return context;
    }
}
